/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_EXT_CONNECTOR_TUNTAP_H
#define PACKETEER_EXT_CONNECTOR_TUNTAP_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/connector/types.h>

namespace packeteer::ext {

/**
 * Registers a connector type for TUN or TAP devices, where the
 * platform supports them.
 *
 * The device type is selected via either the "tun" or "tap" schemes.
 * The path part is intended to provide the chosen device name.
 *
 * Note: the register function will register two connector types, one for
 * TUN and one for TAP connectors. That means the given register_as parameter
 * and the parameter plus one are being allocated here.
 *
 * Query parameters you can set are:
 * - mtu (integer)
 * - txqueuelen (integer)
 *
 * Examples:
 *   1. auto conn = connector{api, "tun:///foo"};
 *   2. auto conn = connector{api, "tap:///bar"};
 */
PACKETEER_API
error_t
register_connector_tuntap(std::shared_ptr<packeteer::api> api,
    connector_type register_as = packeteer::CT_USER);

} // namespace packeteer::ext

#endif // guard
