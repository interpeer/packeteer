/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "../../test/env.h"

#include <packeteer/ext/connector/tuntap.h>
#include <packeteer/connector.h>

namespace p7r = packeteer;

namespace {

inline auto
api_if_implemented()
{
  auto api = p7r::api::create();
  auto err = p7r::ext::register_connector_tuntap(api);
  if (err == p7r::ERR_NOT_IMPLEMENTED) {
    return std::shared_ptr<p7r::api>{};
  }
  return api;
}

} // anonymoys namespace

#define SKIP_IF_NOT_IMPLEMENTED \
  auto api = api_if_implemented(); \
  if (!api) { \
    GTEST_SKIP(); \
    return; \
  }


TEST(ExtConnectorTunTap, tun_create)
{
  SKIP_IF_NOT_IMPLEMENTED;

  ASSERT_NO_THROW(auto conn = p7r::connector(api, "tun:///tun_test"));
}


TEST(ExtConnectorTunTap, tap_create)
{
  SKIP_IF_NOT_IMPLEMENTED;

  ASSERT_NO_THROW(auto conn = p7r::connector(api, "tap:///tap_test"));
}


TEST(ExtConnectorTunTap, tun_listen)
{
  SKIP_IF_NOT_IMPLEMENTED;

  p7r::connector conn;
  ASSERT_NO_THROW(conn = p7r::connector(api, "tun:///tun_test"));

  ASSERT_FALSE(conn.listening());
  ASSERT_FALSE(conn.connected());

  // Listen or connect - it's the same on this connector

  auto err = conn.listen();
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  ASSERT_TRUE(conn.listening());
  ASSERT_TRUE(conn.connected());
}


TEST(ExtConnectorTunTap, tap_listen)
{
  SKIP_IF_NOT_IMPLEMENTED;

  p7r::connector conn;
  ASSERT_NO_THROW(conn = p7r::connector(api, "tap:///tap_test"));

  ASSERT_FALSE(conn.listening());
  ASSERT_FALSE(conn.connected());

  // Listen or connect - it's the same on this connector

  auto err = conn.listen();
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  ASSERT_TRUE(conn.listening());
  ASSERT_TRUE(conn.connected());
}


TEST(ExtConnectorTunTap, auto_select_name)
{
  SKIP_IF_NOT_IMPLEMENTED;

  p7r::connector conn;
  ASSERT_NO_THROW(conn = p7r::connector(api, "tun:///?mtu=200"));
  ASSERT_EQ("tun", conn.peer_addr().scheme());
  ASSERT_EQ(liberate::net::socket_address{"/"}, conn.peer_addr().socket_address());

  // Listen fills in the socket address with the actual device name being
  // used.
  auto err = conn.listen();
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ("tun", conn.peer_addr().scheme());
  ASSERT_NE(liberate::net::socket_address{"/"}, conn.peer_addr().socket_address());
  auto url = conn.connect_url();
  ASSERT_GT(url.path.size(), 1);
  ASSERT_NE(url.query.end(), url.query.find("mtu"));
}
