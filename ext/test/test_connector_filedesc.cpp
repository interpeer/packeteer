/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "../../test/env.h"

#include <packeteer/ext/connector/filedesc.h>
#include <packeteer/connector.h>

namespace p7r = packeteer;

TEST(ExtConnectorFileDesc, raise_without_registration)
{
  ASSERT_THROW(auto conn = p7r::connector(test_env->api, "filedesc:///stdin"),
    p7r::exception);
}


TEST(ExtConnectorFileDesc, succeed_with_registration)
{
  auto api = packeteer::api::create();
  p7r::ext::register_connector_filedesc(api);
  ASSERT_NO_THROW(auto conn = p7r::connector(api, "filedesc:///stdin"));
  ASSERT_NO_THROW(auto conn = p7r::connector(api, "fd:///stdin"));
}


TEST(ExtConnectorFileDesc, duplicate_anon)
{
  auto api = packeteer::api::create();
  p7r::ext::register_connector_filedesc(api);

  auto anon = p7r::connector{api, "anon://?blocking=true"};
  anon.connect();
  ASSERT_TRUE(anon.connected());
  ASSERT_TRUE(anon.is_blocking());

  // Duplicate read FD
  std::string url = "fd:///" + std::to_string(anon.get_read_handle().sys_handle()) + "?blocking=true";

  auto fd = p7r::connector{api, url};
  ASSERT_TRUE(fd.connected());
  ASSERT_TRUE(fd.is_blocking());

  // Write to anon must make fd readable
  std::string buf{"Hello, world!"};
  size_t written = 0;
  auto err = anon.write(buf.c_str(), buf.length(), written);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(written, buf.length());

  char rb[200] = { 0 };
  size_t did_read = 0;
  err = fd.read(rb, sizeof(rb), did_read);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(did_read, written);

  std::string res{rb, rb + did_read};
  ASSERT_EQ("Hello, world!", res);
}

// TODO test with wrapping a pipe descriptor; we should
// just be able to use them interchangeably
