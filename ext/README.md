Packeteer Extensions
====================

This optional library contains extension connectors for packeteer. The folliwng
connectors are included.

File Descriptors (POSIX only)
-----------------------------

Sometimes, a library provides you with a system specific file descriptor that
you need to include in your I/O operations. Enter the `filedesc` or `fd`
connectors.

The connector URL syntax is pretty simple; you specify the scheme followed by
the file descriptor. As in other connectors, you can select whether the
descriptor should be set to `blocking` mode or not.

Example: `fd:///42?blocking=true`

As a special help, the scheme also understands names for some standard file
descriptors. If you specify `stdin`, `stdout` or `stderr`, they will be
translated to `STDIN_FILENO`, `STDOUT_FILENO` or `STDERR_FILENO` respectively.

Using this mechanism, it's possible to use packeteer's I/O scheduling
facilities in software reading from/writing to shell pipes.

TUN/TAP (Linux only)
--------------------

For implementation of VPNs or similar software, it is desirable to create
virtual network devices which capture routed traffic. Reading from these must
be efficient, and is therefore a good use of packeteer's I/O scheduling
facilities.

The `tun` and `tap` URL schemes create appropriate devices. If given a (path)
name, that name will be provided to the kernel to name the network device. If
no name is given, the kernel will be requested to choose a network device name,
which is then returned and becomes part of the connection URL.

The scheme supports an optional `mtu` parameter with which to set the maximum
transfer unit size in Bytes for the network device.
