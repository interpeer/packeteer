/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_INTERFACE_H
#define PACKETEER_CONNECTOR_INTERFACE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/handle.h>
#include <packeteer/error.h>

#include <packeteer/connector/types.h>
#include <packeteer/connector/peer_address.h>

#include <liberate/net/socket_address.h>

namespace packeteer {

/**
 * Base class for connector implementations. See the connector proxy class
 * in the main namespace for details.
 **/
struct PACKETEER_API connector_interface
{
public:
  /***************************************************************************
   * Always to be implemented by child classes
   **/

  /**
   * Expected to close() the connector.
   */
  virtual ~connector_interface() {};

  virtual error_t listen() = 0;
  virtual bool listening() const = 0;

  virtual error_t connect() = 0;
  virtual bool connected() const = 0;

  /**
   * The accept() call *may* return this. The connector proxy class will
   * take care of reference counting such that the connector_interface
   * instance will be deleted only when all connector instances referring to
   * it are gone.
   */
  virtual connector_interface * accept(liberate::net::socket_address & addr) = 0;

  virtual handle get_read_handle() const = 0;
  virtual handle get_write_handle() const = 0;

  virtual error_t close() = 0;

  /***************************************************************************
   * (Abstract) setting accessors
   **/
  /**
   * Retrieve connector options. is_blocking() may return the blocking mode
   * set on a file descriptor instead of from the options.
   */
  virtual connector_options get_options() const = 0;
  virtual bool is_blocking() const = 0;

  /**
   * Retrieve the peer_address this connector is representing.
   */
  virtual peer_address peer_addr() const = 0;

  /***************************************************************************
   * Default (POSIX-oriented) implementations; may be subclassed if necessary.
   **/
  virtual error_t receive(void * buf, size_t bufsize, size_t & bytes_read,
      ::liberate::net::socket_address & sender) = 0;
  virtual error_t send(void const * buf, size_t bufsize, size_t & bytes_written,
      ::liberate::net::socket_address const & recipient) = 0;
  virtual size_t peek() const = 0;

  virtual error_t read(void * buf, size_t bufsize, size_t & bytes_read) = 0;
  virtual error_t write(void const * buf, size_t bufsize, size_t & bytes_written) = 0;
};

} // namespace packeteer

#endif // guard
