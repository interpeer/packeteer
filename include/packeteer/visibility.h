/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_VISIBILITY_H
#define PACKETEER_VISIBILITY_H

#if defined(_WIN32) || defined(__CYGWIN__) || defined(__MINGW32__)
  #if defined(PACKETEER_IS_BUILDING) && PACKETEER_IS_BUILDING > 0
    // We can always dllexport, even when building static libraries.
    #define PACKETEER_API __declspec(dllexport)
  #else
    // If we're not building, this header is included in a project that
    // uses the library.
    #if defined(PACKETEER_STATIC)
      // Linking against static libraries searches for the symbol without
      // dllimport.
      #define PACKETEER_API
    #else
      // Linking against dynamic libraries searches for the symbol with
      // dllimport.
      #define PACKETEER_API __declspec(dllimport)
    #endif
  #endif
  #define PACKETEER_API_FRIEND PACKETEER_API
#else // Windows
  #if __GNUC__ >= 4
    #define PACKETEER_API  [[gnu::visibility("default")]]
  #else
    #define PACKETEER_API
  #endif // GNU C
  #define PACKETEER_API_FRIEND
#endif // POSIX

// Private symbols may be exported in debug builds for testing purposes.
#if defined(DEBUG)
  #define PACKETEER_PRIVATE PACKETEER_API
#else
  #define PACKETEER_PRIVATE
#endif

#endif // guard
