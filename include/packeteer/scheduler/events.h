/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_EVENTS_H
#define PACKETEER_SCHEDULER_EVENTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

namespace packeteer {

// Events are masked together into events_t. All event flags with values equal
// to or higher than PEV_USER are user-defined event types.
using events_t = uint32_t;


// Event types for which callback functions can be registered. The callback
// will be notified for which event(s) it was invoked.
enum PACKETEER_API : events_t
{
  PEV_IO_READ    = (1 <<  0),  // A handle is ready for reading
  PEV_IO_WRITE   = (1 <<  1),  // A handle is ready for writing
  PEV_IO_ERROR   = (1 <<  2),  // A handle has produced errors
  PEV_IO_OPEN    = (1 <<  3),  // A handle has been opened. This event only
                               // gets sent on some platforms.
  PEV_IO_CLOSE   = (1 <<  4),  // A handle has been closed. This event
                               // cannot be reliably reported, consider it
                               // informative only.

  PEV_TIMEOUT    = (1 <<  7),  // A timout has been reached that the callback was
                               // registered for.
  PEV_ERROR      = (1 <<  8),  // Internal scheduler error.

  PEV_ALL_BUILTIN = PEV_IO_READ | PEV_IO_WRITE | PEV_IO_ERROR | PEV_IO_OPEN
      | PEV_IO_CLOSE | PEV_TIMEOUT | PEV_ERROR,

  PEV_USER       = (1 << 15), // A user-defined event was fired (see below).
};

} // namespace packeteer

#endif // guard
