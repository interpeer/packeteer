/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_TYPES_H
#define PACKETEER_SCHEDULER_TYPES_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <chrono>

namespace packeteer {

/**
 * Types used in the scheduler, that may be of use elsewhere.
 **/
using duration = std::chrono::nanoseconds;
using clock    = std::chrono::steady_clock;

template <typename durationT = duration>
using clock_time_point = std::chrono::time_point<clock, durationT>;

using time_point = clock_time_point<duration>;

/**
 * I/O callbacks can have option flags associated with them.
 */
using io_flags_t = uint8_t;

enum io_flags : io_flags_t
{
  IO_FLAGS_NONE     = 0,
  IO_FLAGS_ONESHOT  = (1 << 0),   //! Unschedule after triggered once
  IO_FLAGS_REPEAT   = (1 << 1),   //! Reschedule if callback returns
                                  //! ERR_REPEAT_ACTION. Implies unscheduling
                                  //! otherwise.
};



} // namespace packeteer

#endif // guard
