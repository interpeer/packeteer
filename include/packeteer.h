/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_PACKETEER_H
#define PACKETEER_PACKETEER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <memory>

/**
 * Which platform are we on?
 **/
#if !defined(PACKETEER_PLATFORM_DEFINED)
#  if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#    define PACKETEER_WIN32
#  else
#    define PACKETEER_POSIX
#  endif
#  define PACKETEER_PLATFORM_DEFINED
#endif

// We want standard int types across the board.
#include <liberate.h>
#include <liberate/types.h>

// Visibility macros are used by all, so they must come first.
#include <packeteer/visibility.h>

// Not all, but the very basic headers are always included.
#include <packeteer/error.h>
#include <packeteer/version.h>

/**
 * Decide what to include globally
 **/
#if defined(PACKETEER_WIN32)
// Include windows.h with minimal definitions
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    define __UNDEF_LEAN_AND_MEAN
#  endif
#  define NOMINMAX
// Unicode builds
#  define UNICODE
#  define _UNICODE
#  include <windows.h>
#  include <WinDef.h>
#  ifdef __UNDEF_LEAN_AND_MEAN
#    undef WIN32_LEAN_AND_MEAN
#    undef __UNDEF_LEAN_AND_MEAN
#  endif
// Link against winsock2
#  pragma comment(lib, "Ws2_32.lib")
#endif


namespace packeteer {

/**
 * Forward
 */
class PACKETEER_API registry;
class PACKETEER_API resolver;


/**
 * The primary entry point into a packeteer library instance.
 */
class PACKETEER_API api
{
public:
  /**
   * Create a new API instance.
   */
  static std::shared_ptr<api> create();
  ~api();

  /**
   * Don't copy the instance - share it around, or create a new one.
   */
  api(api &&) = delete;
  api(api const &) = delete;
  api & operator=(api const &) = delete;

  /**
   * Access internal interfaces
   */
  ::packeteer::registry & reg();
  ::packeteer::registry & registry();

  ::packeteer::resolver & res();
  ::packeteer::resolver & resolver();

  ::liberate::api & liberate();

private:
  api();
  void init(std::weak_ptr<api> self);

  struct api_impl;
  std::unique_ptr<api_impl> m_impl;
};

} // namespace packeteer

#endif // guard
