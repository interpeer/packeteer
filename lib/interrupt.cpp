/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <packeteer.h>

#include "interrupt.h"

namespace packeteer::detail {


void set_interrupt(connector & signal)
{
  char buf[1] = { '\0' };
  size_t amount = 0;
  signal.write(buf, sizeof(buf), amount);
}



bool clear_interrupt(connector & signal)
{
  char buf[1] = { '\0' };
  size_t amount = 0;
  signal.read(buf, sizeof(buf), amount);
  return (amount > 0); //!OCLINT
}


} // namespace packeteer::detail
