/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_GLOBALS_H
#define PACKETEER_GLOBALS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <build-config.h>

/*****************************************************************************
 * *Internal* global definitions.
 **/

/**
 * ::listen() has a backlog parameter that determines how many pending
 * connections the kernel should keep. Widely discussed as common is the value
 * 128, so we'll use that.
 **/
#define PACKETEER_LISTEN_BACKLOG  128


/**
 * Maximum number of events to read with Epoll/KQueue/IOCP at a single call.
 **/
#define PACKETEER_EPOLL_MAXEVENTS   PACKETEER_EVENT_MAX
#define PACKETEER_KQUEUE_MAXEVENTS  PACKETEER_EVENT_MAX
#define PACKETEER_IOCP_MAXEVENTS    PACKETEER_EVENT_MAX

#endif // guard
