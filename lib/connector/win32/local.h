/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_WIN32_LOCAL_H
#define PACKETEER_CONNECTOR_WIN32_LOCAL_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/connector/interface.h>

#include "socket.h"

namespace packeteer::detail {

/**
 * Wrapper around the pipe class.
 **/
struct connector_local : public ::packeteer::detail::connector_socket
{
public:
  connector_local(peer_address const & addr,
      connector_options const & options);
  virtual ~connector_local();

  error_t listen() override;

  bool listening() const override;
  bool connected() const override;

  handle get_read_handle() const override;
  handle get_write_handle() const override;

  error_t connect() override;

  connector_interface * accept(liberate::net::socket_address & addr) override;

  error_t close() override;

private:
  connector_local();

  // File system entry owner.
  bool m_owner = false;

  ::packeteer::handle::sys_handle_t m_other_handle = ::packeteer::handle::INVALID_SYS_HANDLE;
};

} // namespace packeteer::detail

#endif // guard
