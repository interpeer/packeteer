/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include "udp.h"

#include <packeteer/error.h>

#include "../../globals.h"

namespace packeteer::detail {

namespace {

inline int
select_domain(liberate::net::socket_address const & addr)
{
  switch (addr.type()) {
    case liberate::net::AT_INET4:
      return AF_INET;

    case liberate::net::AT_INET6:
      return AF_INET6;

    default:
      throw exception(ERR_INVALID_VALUE, "Expected IPv4 or IPv6 address!");
  }
}

} // anonymous namespace

connector_udp::connector_udp(peer_address const & addr,
    connector_options const & options)
  : connector_socket{addr, options}
{
}



connector_udp::~connector_udp()
{
  connector_udp::close();
}



error_t
connector_udp::connect()
{
  return connector_socket::socket_connect(
      select_domain(m_address.socket_address()),
      SOCK_DGRAM, IPPROTO_UDP);
}



error_t
connector_udp::listen()
{
  // Attempt to bind
  handle::sys_handle_t handle = handle::INVALID_SYS_HANDLE;
  error_t err = connector_socket::socket_bind(
      select_domain(m_address.socket_address()),
      SOCK_DGRAM, IPPROTO_UDP, handle);
  if (ERR_SUCCESS != err) {
    return err;
  }

  // Finally, set the handle
  m_handle = handle;
  m_server = true;

  return ERR_SUCCESS;
}



error_t
connector_udp::close()
{
  return connector_socket::socket_close();
}


connector_interface *
connector_udp::accept(liberate::net::socket_address & /* addr*/)
{
  if (!listening()) {
    return nullptr;
  }
  return this;
}


} // namespace packeteer::detail
