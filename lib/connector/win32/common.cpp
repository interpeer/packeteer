/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <packeteer/connector/interface.h>

#include "common.h"
#include "io_operations.h"
#include "../../macros.h"

namespace packeteer::detail {

connector_common::connector_common(peer_address const & addr,
    connector_options const & options)
  : m_options(options)
  , m_address{addr}
{
  DLOG("connector_common::connector_common(" << options << ")");
}



connector_common::~connector_common()
{
}



error_t
connector_common::read(void * buf, size_t bufsize, size_t & bytes_read)
{
  if (!connected() && !listening()) {
    return ERR_INITIALIZATION;
  }

  ssize_t read = -1;
  auto err = detail::read(get_read_handle(), buf, bufsize, read);
  if (ERR_SUCCESS == err) {
    bytes_read = read;
  }
  return err;
}



error_t
connector_common::write(void const * buf, size_t bufsize, size_t & bytes_written)
{
  if (!connected() && !listening()) {
    return ERR_INITIALIZATION;
  }

  ssize_t written = -1;
  auto err = detail::write(get_write_handle(), buf, bufsize, written);
  if (ERR_SUCCESS == err) {
    bytes_written = written;
  }
  return err;
}



connector_options
connector_common::get_options() const
{
  return m_options;
}


peer_address
connector_common::peer_addr() const
{
  return m_address;
}


} // namespace packeteer::detail
