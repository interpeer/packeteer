/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_CONNECTORS_H
#define PACKETEER_CONNECTOR_CONNECTORS_H

#include <build-config.h>

// POSIX vs Windows includes
#if defined(PACKETEER_POSIX)
#  include "posix/anon.h"
#  include "posix/tcp.h"
#  include "posix/udp.h"
#  include "posix/local.h"
#  include "posix/fifo.h"
#else
#  include "win32/anon.h"
#  include "win32/tcp.h"
#  include "win32/udp.h"
#  if defined(PACKETEER_HAVE_AFUNIX_H)
#    include "win32/local.h"
#  endif
#  include "win32/pipe.h"
#endif


#endif // guard
