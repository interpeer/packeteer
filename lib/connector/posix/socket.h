/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_POSIX_SOCKET_H
#define PACKETEER_CONNECTOR_POSIX_SOCKET_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include "common.h"

namespace packeteer::detail {

/**
 * Base for socket-style I/O on POSIX systems
 **/
struct connector_socket : public ::packeteer::detail::connector_common
{
public:
  connector_socket(peer_address const & addr,
      connector_options const & options);

  // Connector interface, partially implemented
  bool listening() const override;
  bool connected() const override;

  handle get_read_handle() const override;
  handle get_write_handle() const override;

  bool is_blocking() const override;

  // Socket-specific versions of connect() and accept()
  error_t socket_create(int domain, int type, int & fd);
  error_t socket_bind(int domain, int type, int & fd);
  error_t socket_listen(int fd);
  error_t socket_connect(int domain, int type);
  error_t socket_accept(int & new_fd, liberate::net::socket_address & addr);
  error_t socket_close();

protected:
  bool                              m_server = false;
  bool                              m_connected = false;
  int                               m_fd = -1;
};

} // namespace packeteer::detail

#endif // guard
