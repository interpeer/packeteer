/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_POSIX_FD_H
#define PACKETEER_CONNECTOR_POSIX_FD_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/handle.h>

namespace packeteer::detail {

/**
 * Set/get blocking modes on file descriptors.
 */
error_t
set_blocking_mode(handle::sys_handle_t const & fd, bool state = false);

error_t
get_blocking_mode(handle::sys_handle_t const & fd, bool & state);


/**
 * Ensure handle has O_CLOEXEC set. We always set it, so let's not complicate
 * matters.
 */
error_t
set_close_on_exit(handle::sys_handle_t const & fd);


} // namespace packeteer::detail

#endif // guard
