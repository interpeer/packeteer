/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_POSIX_FIFO_H
#define PACKETEER_CONNECTOR_POSIX_FIFO_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/handle.h>

#include "common.h"

namespace packeteer::detail {

/**
 * Wrapper around the fifo class.
 **/
struct connector_fifo : public connector_common
{
public:
  connector_fifo(peer_address const & addr,
      connector_options const & options);
  ~connector_fifo();

  error_t listen() override;
  bool listening() const override;

  error_t connect() override;
  bool connected() const override;

  connector_interface * accept(liberate::net::socket_address & addr) override;

  handle get_read_handle() const override;
  handle get_write_handle() const override;

  error_t close() override;

  bool is_blocking() const override;

private:
  connector_fifo();

  bool                              m_server = false;
  bool                              m_owner = false;
  bool                              m_connected = false;
  ::packeteer::handle               m_handle = {};
};

} // namespace packeteer::detail

#endif // guard
