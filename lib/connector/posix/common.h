/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_CONNECTOR_POSIX_COMMON_H
#define PACKETEER_CONNECTOR_POSIX_COMMON_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/connector/interface.h>

namespace packeteer::detail {

/**
 * TCP connector
 **/
struct connector_common : public ::packeteer::connector_interface
{
public:
  explicit connector_common(peer_address const & addr,
      connector_options const & options);
  virtual ~connector_common();

  // Common connector implementations
  error_t receive(void * buf, size_t bufsize, size_t & bytes_read,
      ::liberate::net::socket_address & sender) override;
  error_t send(void const * buf, size_t bufsize, size_t & bytes_written,
      ::liberate::net::socket_address const & recipient) override;
  size_t peek() const override;

  error_t read(void * buf, size_t bufsize, size_t & bytes_read) override;
  error_t write(void const * buf, size_t bufsize, size_t & bytes_written) override;

  connector_options get_options() const override;
  peer_address peer_addr() const override;
protected:
  // Derived classes should be able to ues these directly.
  connector_options m_options;
  peer_address      m_address;

private:
  connector_common();
};

} // namespace packeteer::detail

#endif // guard
