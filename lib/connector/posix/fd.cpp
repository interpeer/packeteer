/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include "fd.h"

#include "../../macros.h"

#include <fcntl.h>
#include <unistd.h>

namespace packeteer::detail {

namespace {

inline error_t
translate_fcntl_errno()
  OCLINT_SUPPRESS("high cyclomatic complexity")
{
  ERRNO_LOG("fcntl error");
  switch (errno) {
    case EBADF:
    case EINVAL:
      return ERR_INVALID_VALUE;

    case EFAULT:
      return ERR_OUT_OF_MEMORY;

    case EACCES:
    case EAGAIN:
    case EINTR:
    case EBUSY:
    case EDEADLK:
    case EMFILE:
    case ENOLCK:
    case ENOTDIR:
    case EPERM:
      // TODO this is probably correct for all of these cases. check again?
      // EINTR should not occur for F_GETFL/F_SETFL; check this
      // function if it's used differently in future.
      // EACCESS/EAGAIN means the file descriptor is locked; we'll
      // ignore special treatment for the moment.
    default:
      return ERR_UNEXPECTED;
  }
}

} // anonymous namespace


error_t
set_blocking_mode(handle::sys_handle_t const & fd, bool state /* = false */)
{
  int val = ::fcntl(fd, F_GETFL, 0);
  if (-1 == val) {
    return translate_fcntl_errno();
  }

  val |= O_CLOEXEC;
  if (state) {
    val &= ~O_NONBLOCK;
  }
  else {
    val |= O_NONBLOCK;
  }
  val = ::fcntl(fd, F_SETFL, val);
  if (-1 == val) {
    ::close(fd);
    return translate_fcntl_errno();
  }

  return ERR_SUCCESS;
}


error_t
get_blocking_mode(handle::sys_handle_t const & fd, bool & state)
{
  int val = ::fcntl(fd, F_GETFL, 0);
  if (-1 == val) {
    return translate_fcntl_errno();
  }

  state = !bool(val & O_NONBLOCK);
  return ERR_SUCCESS;
}



error_t
set_close_on_exit(handle::sys_handle_t const & fd)
{
  int val = ::fcntl(fd, F_GETFL, 0);
  if (-1 == val) {
    return translate_fcntl_errno();
  }

  val |= O_CLOEXEC;
  val = ::fcntl(fd, F_SETFL, val);
  if (-1 == val) {
    ::close(fd);
    return translate_fcntl_errno();
  }

  return ERR_SUCCESS;
}



} // namespace packeteer::detail
