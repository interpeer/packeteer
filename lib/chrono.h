/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PACKETEER_THREAD_CHRONO_H
#define PACKETEER_THREAD_CHRONO_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#ifdef PACKETEER_HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef PACKETEER_HAVE_TIME_H
#include <time.h>
#endif

#ifdef PACKETEER_WIN32
#include "net/netincludes.h"
#define timeval TIMEVAL
#endif

#include <chrono>

#include <packeteer/error.h>

namespace packeteer::thread::chrono {

template <typename inT, typename outT>
inline void convert(inT const &, outT &)
{
  throw packeteer::exception(ERR_UNEXPECTED, "Conversion from "
      "std::chrono::duration to selected type not implemented.!");
}

#if defined(PACKETEER_HAVE_SYS_TIME_H) || defined(PACKETEER_WIN32)
template <typename inT>
inline void convert(inT const & in, ::timeval & val)
{
  auto repr = ::std::chrono::round<::std::chrono::microseconds>(in).count();

  val.tv_sec = time_t(repr / 1'000'000);
  val.tv_usec = repr % 1'000'000;
}
#endif


#ifdef PACKETEER_HAVE_TIME_H
template <typename inT>
inline void convert(inT const & in, ::timespec & val)
{
  auto repr = ::std::chrono::round<::std::chrono::nanoseconds>(in).count();

  val.tv_sec = time_t(repr / 1'000'000'000);
  val.tv_nsec = repr % 1'000'000'000;
}
#endif


} // namespace packeteer::thread::chrono

#endif // guard
