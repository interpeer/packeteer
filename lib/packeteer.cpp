/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <packeteer.h>

#include <chrono>

#include <stdlib.h>

#include <packeteer/registry.h>
#include <packeteer/resolver.h>

#include "macros.h"

namespace packeteer {

struct api::api_impl
{
  explicit api_impl(std::weak_ptr<api> api)
    : reg{api}
    , res{api}
  {
  }

  ::liberate::api       liberate = {};
  ::packeteer::registry reg;
  ::packeteer::resolver res;
};



std::shared_ptr<api>
api::create()
{
  auto ret = std::shared_ptr<api>(new api());
  ret->init(ret);
  return ret;
}



api::api()
{
}



void
api::init(std::weak_ptr<api> self)
{
  m_impl = std::make_unique<api_impl>(self);

  // Initialize random seed used in connector construction.
  auto now = std::chrono::steady_clock::now().time_since_epoch();
  srand(now.count());
}



api::~api()
{
}



::packeteer::registry &
api::reg()
{
  return m_impl->reg;
}



::packeteer::registry &
api::registry()
{
  return m_impl->reg;
}



::packeteer::resolver &
api::res()
{
  return m_impl->res;
}



::packeteer::resolver &
api::resolver()
{
  return m_impl->res;
}



::liberate::api &
api::liberate()
{
  return m_impl->liberate;
}



} // namespace packeteer
