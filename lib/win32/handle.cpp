/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/handle.h>

#include "sys_handle.h"

namespace packeteer {


handle::sys_handle_t
handle::sys_make_dummy(size_t const & value)
{
  return std::make_shared<handle::opaque_handle>(reinterpret_cast<HANDLE>(value));
}



size_t
handle::sys_handle_hash(sys_handle_t const & handle)
{
  char const * p = reinterpret_cast<char const *>(&handle->handle);
  size_t state = std::hash<char>()(p[0]);
  for (size_t i = 1 ; i < sizeof(HANDLE) ; ++i) {
    liberate::cpp::hash_combine(state, p[i]);
  }
  return state;
}



bool
handle::sys_equal(sys_handle_t const & first, sys_handle_t const & second)
{
  if (!first && !second) {
    return true;
  }
  if (!first || !second) {
    return false;
  }
  return first->handle == second->handle;
}


bool
handle::sys_less(sys_handle_t const & first, sys_handle_t const & second)
{
  if (!first && !second) {
    return false;
  }
  if (!first && second) {
    return true;
  }
  if (first && !second) {
    return false;
  }
  return first->handle < second->handle;
}

} // namespace packeteer
