/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_IO_WIN32_IOCP_H
#define PACKETEER_SCHEDULER_IO_WIN32_IOCP_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#if !defined(PACKETEER_HAVE_IOCP)
#error I/O completion ports not detected
#endif

#include <unordered_set>

#include <packeteer/scheduler/events.h>

#include "../../io.h"

namespace packeteer::detail {

// I/O subsystem based on I/O completion ports.
struct PACKETEER_PRIVATE io_iocp : public io
{
public:
  explicit io_iocp(std::shared_ptr<api> const & api);
  ~io_iocp();

  virtual void
  register_connector(connector const & conn, events_t const & events) override;

  virtual void
  register_connectors(connector const * conns, size_t size,
      events_t const & events) override;

  virtual void
  unregister_connector(connector const & conn, events_t const & events) override;

  virtual void
  unregister_connectors(connector const * conns, size_t size,
      events_t const & events) override;

  virtual void wait_for_events(io_events & events,
      duration const & timeout) override;

private:
  /***************************************************************************
   * Data
   **/
  HANDLE                      m_iocp = INVALID_HANDLE_VALUE;
  std::unordered_set<HANDLE>  m_associated = {};
};


} // namespace packeteer::detail

#endif // guard
