/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_IO_POSIX_KQUEUE_H
#define PACKETEER_SCHEDULER_IO_POSIX_KQUEUE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#if !defined(PACKETEER_HAVE_KQUEUE)
#error KQueue not detected
#endif

#include <packeteer/scheduler/events.h>

#include "../../io.h"

namespace packeteer::detail {

// I/O subsystem based on select.
struct io_kqueue : public io
{
public:
  explicit io_kqueue(std::shared_ptr<api> api);
  ~io_kqueue();

  virtual void register_connector(connector const & conn, events_t const & events) override;
  virtual void register_connectors(connector const * conns, size_t amount,
      events_t const & events) override;

  virtual void unregister_connector(connector const & conn, events_t const & events) override;
  virtual void unregister_connectors(connector const * conns, size_t amount,
      events_t const & events) override;

  virtual void wait_for_events(io_events & events,
      packeteer::duration const & timeout) override;

private:
  /***************************************************************************
   * Data
   **/
  int m_kqueue_fd;
};


} // namespace packeteer::detail

#endif // guard
