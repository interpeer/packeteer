/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_WORKER_H
#define PACKETEER_SCHEDULER_WORKER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <thread>

#include <liberate/concurrency/concurrent_queue.h>
#include <liberate/concurrency/tasklet.h>

#include "scheduler_impl.h"

namespace packeteer::detail {

/*****************************************************************************
 * Implements a worker thread for the scheduler implementation.
 **/
class worker
  : public liberate::concurrency::tasklet
{
public:
  /*****************************************************************************
   * Interface
   **/
  /**
   * The worker thread sleeps waiting for an event on the condition, and wakes
   * up to check the work queue for work to execute.
   **/
  worker(
      liberate::concurrency::tasklet::sleep_condition * condition,
      work_queue_t & work_queue,
      scheduler_command_queue_t & command_queue);
  ~worker();


private:
  /**
   * Sleep()s, runs drain_work_queue() and sleeps again.
   **/
  void worker_loop(liberate::concurrency::tasklet::context & ctx);

  work_queue_t &              m_work_queue;
  scheduler_command_queue_t & m_command_queue;
};

} // namespace packeteer::detail

#endif // guard
