/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2011 Jens Finkhaeuser.
 * Copyright (c) 2012-2014 Unwesen Ltd.
 * Copyright (c) 2015-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_SCHEDULER_IO_H
#define PACKETEER_SCHEDULER_IO_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <vector>
#include <chrono>
#include <unordered_map>

#include <packeteer/connector.h>
#include <packeteer/scheduler/types.h>
#include <packeteer/scheduler/events.h>

namespace packeteer::detail {

/**
 * Events are reported with this structure.
 */
struct io_event
{
  packeteer::connector connector;
  events_t  events;
};

using io_events = std::vector<io_event>;

/**
 * Virtual base class for I/O subsystem. If you override the (un-)register*
 * functions, be sure to call the base implementation if you need m_sys_handles
 * to be updated.
 **/
struct io
{
public:
  explicit io(std::shared_ptr<api> const & api)
    : m_api(api)
  {
  }

  virtual ~io() {};


  virtual void
  register_connector(connector const & conn, events_t const & events)
  {
    m_sys_handles[conn.get_read_handle().sys_handle()] |= events & ~PEV_IO_WRITE;
    m_sys_handles[conn.get_write_handle().sys_handle()] |= events & ~PEV_IO_READ;

    m_connectors[conn.get_read_handle().sys_handle()] = conn;
    m_connectors[conn.get_write_handle().sys_handle()] = conn;
  }



  virtual void
  register_connectors(connector const * conns, size_t size,
      events_t const & events)
  {
    for (size_t i = 0 ; i < size ; ++i) {
      m_sys_handles[conns[i].get_read_handle().sys_handle()] |= events & ~PEV_IO_WRITE;
      m_sys_handles[conns[i].get_write_handle().sys_handle()] |= events & ~PEV_IO_READ;

      m_connectors[conns[i].get_read_handle().sys_handle()] = conns[i];
      m_connectors[conns[i].get_write_handle().sys_handle()] = conns[i];
    }
  }



  virtual void
  unregister_connector(connector const & conn, events_t const & events)
  {
    clear_sys_handle_events(conn.get_read_handle().sys_handle(),
        events & ~PEV_IO_WRITE);
    clear_sys_handle_events(conn.get_write_handle().sys_handle(),
        events & ~PEV_IO_READ);
  }



  virtual void
  unregister_connectors(connector const * conns, size_t size,
      events_t const & events)
  {
    for (size_t i = 0 ; i < size ; ++i) {
      clear_sys_handle_events(conns[i].get_read_handle().sys_handle(),
          events & ~PEV_IO_WRITE);
      clear_sys_handle_events(conns[i].get_write_handle().sys_handle(),
          events & ~PEV_IO_READ);
    }
  }

  virtual void wait_for_events(io_events & events,
      packeteer::duration const & timeout) = 0;


  typedef std::unordered_map<handle::sys_handle_t, events_t> sys_events_map;

protected:
  std::shared_ptr<api> m_api;

  sys_events_map                                      m_sys_handles;
  std::unordered_map<handle::sys_handle_t, connector> m_connectors;

private:
  inline void
  clear_sys_handle_events(handle::sys_handle_t sys_handle, events_t const & events)
  {
    auto iter = m_sys_handles.find(sys_handle);
    if (iter == m_sys_handles.end()) {
      m_connectors.erase(sys_handle);
      return;
    }
    iter->second &= ~events;
    if (!iter->second) {
      m_sys_handles.erase(iter);
      m_connectors.erase(sys_handle);
    }
  }
};


} // namespace packeteer::detail

#endif // guard
