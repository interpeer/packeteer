/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_FLOW_IMPL_H
#define PACKETEER_UTIL_FLOW_IMPL_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <build-config.h>

#include <packeteer/connector.h>

#include <memory>

#include <liberate/logging.h>

#include <packeteer/util/flow.h>
#include <packeteer/util/io_queue.h>

#include "writer.h"

namespace packeteer::util {

using flow_impl_close_func = std::function<error_t (flowid const &)>;

/**
 * The flow implementation manages I/O queues and write re-attempts (the
 * latter via the writer helper).
 *
 * Since flows are pimpl'd, copies will share the same implementation.
 */
struct flow::flow_impl
{

  /**
   * A flow maps a flow identifier to a connector.
   */
  inline flow_impl(flowid const & id, connector conn, scheduler & sched,
      timepoint_flow_callback read_cb,
      io_queue_creator creator,
      flow_impl_close_func closer
  )
    : m_id{id}
    , m_conn{conn}
    , m_read_queue{creator()}
    , m_write_queue{creator()}
    , m_writer{sched,
        timepoint_callback{std::bind(&flow_impl::write_cb, this,
          std::placeholders::_1,
          std::placeholders::_2
        )}
      }
    , m_read_cb{read_cb}
    , m_close{closer}
  {
  }



  inline error_t
  read(void * buf, size_t bufsize, size_t & bytes_read)
  {
    // Reading is just from the read queue. If that is empty, there is nothing
    // to read at the moment.
    bytes_read = m_read_queue->pop_front(buf, bufsize);
    if (bytes_read > 0) {
      return ERR_SUCCESS;
    }
    return ERR_REPEAT_ACTION;
  }



  inline error_t
  write(void const * buf, size_t bufsize, size_t & bytes_written)
  {
    // If the current write queue is empty, we should try and write directly to
    // the connector. This saves us a few copies. Only if that fails should we
    // push the data to the write queue.
    auto err = do_write(buf, bufsize, bytes_written);
    if (ERR_SUCCESS == err) {
      return ERR_SUCCESS;
    }

    // Push data onto queue, and pretend we're alright.
    m_write_queue->push_back(buf, bufsize);
    bytes_written = bufsize;

    // There is no point to try and drain the write queue now, but we do need
    // to trigger the writer to wake up and try later.
    m_writer.wait_for_writable(m_conn);

    // Operation will happen asynchronously.
    return ERR_ASYNC;
  }



  inline bool
  add_to_read_queue(time_point const & now, flow const & the_flow, char const * buf, size_t bufsize)
  {
    auto success = m_read_queue->push_back(buf, bufsize);
    if (!success) {
      return false;
    }

    if (m_read_cb) {
      m_read_cb(now, the_flow);
    }

    return true;
  }



  inline error_t
  close()
  {
    return m_close(m_id);
  }

private:
  friend class flow;

  inline error_t
  try_drain_write_queue()
  {
    do {
      char wbuf[PACKETEER_IO_BUFFER_SIZE];
      auto popped = m_write_queue->pop_front(wbuf, sizeof(wbuf));
      if (!popped) {
        // Nothing more to do
        return ERR_SUCCESS;
      }

      // Try writing, factoring out stream vs. datagram differences.
      size_t bytes_written = 0;
      auto err = do_write(wbuf, popped, bytes_written);
      if (ERR_SUCCESS == err) {
        continue;
      }
      if (ERR_REPEAT_ACTION == err) {
        // Ok, nothing to but to push the data back onto the queue.
        m_write_queue->push_front(wbuf, popped);
        return ERR_REPEAT_ACTION;
      }
      break;
    } while (true);

    return ERR_SUCCESS;
  }



  inline error_t
  write_cb(time_point const &, connector)
  {
    // The write callback just tries to drain the write queue. We can ignore
    // the connection parameter, because it's going to be m_conn.
    return try_drain_write_queue();
  }



  inline error_t
  do_write(void const * buffer, size_t bufsize, size_t & bytes_written)
  {
    error_t err = ERR_SUCCESS;

    if (m_conn.get_options() & CO_DATAGRAM) {
      err = m_writer.send(m_conn, buffer, bufsize, bytes_written,
          m_id.remote);
    }
    else {
      err = m_writer.write(m_conn, buffer, bufsize, bytes_written);
    }
    return err;
  }



  flowid                    m_id;
  connector                 m_conn;

  std::unique_ptr<io_queue> m_read_queue = {};
  std::unique_ptr<io_queue> m_write_queue = {};

  writer                    m_writer;
  timepoint_flow_callback   m_read_cb;
  flow_impl_close_func      m_close;
};

} // namespace packeteer::util

#endif // guard
