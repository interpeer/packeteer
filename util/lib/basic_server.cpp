/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/basic_server.h>

#include <packeteer/resolver.h>

#include <liberate/logging.h>

#include "callbacks.h"

namespace packeteer::util {

struct basic_server::server_impl
{
  std::shared_ptr<api>        m_api;
  scheduler &                 m_scheduler;

  timepoint_callback          m_read_cb;
  timepoint_flowid_callback   m_remote_cb = {};
  timepoint_flowid_callback   m_close_cb = {};

  std::set<connector>         m_listening = {};
  std::set<connector>         m_established = {};

  inline server_impl(std::shared_ptr<api> api,
      scheduler & sched,
      timepoint_callback read_cb)
    : m_api{api}
    , m_scheduler{sched}
    , m_read_cb{read_cb}
  {
  }


  inline ~server_impl()
  {
    close_multiple(m_listening, m_listening, true, false);
    close_multiple(m_established, m_established, false, false);
  }



  inline error_t
  listen_on(liberate::net::url const & conn_url)
  {
    LIBLOG_DEBUG("Asked to listen on: " << conn_url);

    // First, resolve the URL
    std::set<liberate::net::url> results;
    auto err = m_api->resolver().resolve(results, conn_url);
    if (ERR_INVALID_VALUE == err) {
      // Maybe bad URL scheme; we can only try to use the connection url
      // directly.
      connector conn{m_api, conn_url};
      return listen_on_connector(conn);
    }

    if (ERR_SUCCESS != err) {
      return err;
    }

    // Otherwise, try for each result
    for (auto url : results) {
      LIBLOG_DEBUG("-> resolves to: " << url);
      connector conn{m_api, url};
      err = listen_on_connector(conn);
      if (ERR_SUCCESS != err) {
        return err;
      }

      m_listening.insert(conn);
    }
    return ERR_SUCCESS;
  }



  inline error_t
  listen_on_connector(connector & conn)
  {
    LIBLOG_DEBUG("Asked to listen on: " << conn);
    if (conn.communicating()) {
      return ERR_INVALID_VALUE;
    }

    auto err = conn.listen();
    if (ERR_SUCCESS != err) {
      return err;
    }

    // Register a close callback.
    callback onclose = [this](time_point const & time, events_t const &,
        connector * iconn) -> error_t
    {
      // No longer interested in read events.
      m_scheduler.unregister_connector(PEV_IO_READ, *iconn);

      m_listening.erase(*iconn);
      if (m_close_cb) {
        // This connector is listening; there is no peer. We're taking the peer
        // address from outside the callback because that's the canonical
        // address we're listening on.
        auto id = flowid{iconn->peer_addr(), {}};
        return m_close_cb(time, id, *iconn);
      }
      return ERR_SUCCESS;
    };
    m_scheduler.register_connector(PEV_IO_CLOSE, conn, onclose,
        IO_FLAGS_ONESHOT);

    // Read and write callbacks differ based on whether the connector is a
    // stream or datagram oriented connector.
    if (conn.get_options() & CO_STREAM) {
      timepoint_flowid_callback onremote = [this](time_point const & time,
          flowid const & id, connector iconn) -> error_t
      {
        m_established.insert(iconn);
        if (m_remote_cb) {
          return m_remote_cb(time, id, iconn);
        }
        return ERR_SUCCESS;
      };

      m_scheduler.register_connector(PEV_IO_READ, conn,
          create_accept_callback(m_scheduler, m_read_cb, onremote,
            m_close_cb));
    }
    else {
      m_scheduler.register_connector(PEV_IO_READ, conn,
          create_read_callback(m_read_cb));
    }

    m_scheduler.commit_callbacks();

    // Remember the listening connector
    m_listening.insert(conn);

    return ERR_SUCCESS;
  }


  inline error_t close(connector conn)
  {
    auto listen_err = close_multiple(
        std::initializer_list<connector>{conn}, m_listening, true);
    auto established_err = close_multiple(
        std::initializer_list<connector>{conn}, m_established, false);

    if (ERR_SUCCESS == listen_err) {
      return ERR_SUCCESS;
    }
    if (ERR_SUCCESS == established_err) {
      return ERR_SUCCESS;
    }
    return ERR_INVALID_VALUE;
  }


  template <typename containerT>
  inline error_t
  close_multiple(containerT const & conns, std::set<connector> & the_set,
      bool listening, bool with_callback = true)
  {
    // Unregister the connectors
    for (auto conn : conns) {
      m_scheduler.unregister_connector(conn);
    }
    try {
      m_scheduler.commit_callbacks();
    } catch (...) {
      // Doesn't really matter; we're *unregistering* after all.
      LIBLOG_DEBUG("Failed to unregister connector during shutdown.");
    }

    // Erase all
    std::set<connector> to_erase;
    for (auto conn : conns) {
      for (auto iter = the_set.begin() ; iter != the_set.end() ; ++iter) {
        if (*iter == conn) {
          to_erase.insert(conn);
        }
      }
    }
    if (to_erase.empty()) {
      return ERR_INVALID_VALUE;
    }

    for (auto conn : to_erase) {
      the_set.erase(conn);
    }

    // Close callbacks
    if (with_callback && m_close_cb) {
      auto now = clock::now();
      for (auto conn : to_erase) {
        if (listening) {
          auto id = flowid{conn.peer_addr(), {}};
          m_close_cb(now, id, conn);
        }
        else {
          auto id = flowid{{}, conn.peer_addr()};
          m_close_cb(now, id, conn);
        }
      }
    }

    // Close all
    error_t ret = ERR_SUCCESS;
    for (auto conn : to_erase) {
      LIBLOG_DEBUG("Server closing: " << conn);
      auto iret = conn.close();
      if (ERR_SUCCESS == ret && ERR_SUCCESS != iret) {
        // First error is reported!
        ret = iret;
      }
    }

    return ret;
  }

};



basic_server::basic_server(std::shared_ptr<api> api, scheduler & sched,
    simple_callback read_cb)
  : m_impl{std::make_shared<basic_server::server_impl>(
      api, sched, erase_timepoint(read_cb)
    )}
{
}


basic_server::basic_server(std::shared_ptr<api> api, scheduler & sched,
    timepoint_callback read_cb)
  : m_impl{std::make_shared<basic_server::server_impl>(
      api, sched, read_cb
    )}
{
}



basic_server &
basic_server::on_remote(simple_flowid_callback remote_cb)
{
  m_impl->m_remote_cb = erase_timepoint(remote_cb);
  return *this;
}


basic_server &
basic_server::on_remote(timepoint_flowid_callback remote_cb)
{
  m_impl->m_remote_cb = remote_cb;
  return *this;
}


basic_server &
basic_server::on_close(simple_flowid_callback close_cb)
{
  m_impl->m_close_cb = erase_timepoint(close_cb);
  return *this;
}


basic_server &
basic_server::on_close(timepoint_flowid_callback close_cb)
{
  m_impl->m_close_cb = close_cb;
  return *this;
}


error_t
basic_server::listen_on(liberate::net::url const & conn_url)
{
  return m_impl->listen_on(conn_url);
}


basic_server &
basic_server::listen(liberate::net::url const & conn_url)
{
  auto err = listen_on(conn_url);
  if (ERR_SUCCESS != err) {
    throw exception(err);
  }
  return *this;
}


std::set<connector> const &
basic_server::listening() const
{
  return m_impl->m_listening;
}


std::size_t
basic_server::num_listening() const
{
  return m_impl->m_listening.size();
}


std::set<connector> const &
basic_server::established() const
{
  return m_impl->m_established;
}


std::size_t
basic_server::num_established() const
{
  return m_impl->m_established.size();
}


error_t
basic_server::close(connector conn)
{
  return m_impl->close(conn);
}

} // namespace packeteer::util
