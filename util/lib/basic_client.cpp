/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/basic_client.h>

#include <list>

#include <packeteer/resolver.h>

#include <liberate/logging.h>

#include "callbacks.h"
#include "resolve.h"

namespace packeteer::util {

struct basic_client::client_impl
{
  std::shared_ptr<api>        m_api;
  scheduler &                 m_scheduler;

  timepoint_callback          m_read_cb;
  timepoint_flowid_callback   m_remote_cb = {};
  timepoint_flowid_callback   m_close_cb = {};

  basic_client::connector_map m_connected;

  inline client_impl(std::shared_ptr<api> api,
      scheduler & sched,
      timepoint_callback read_cb)
    : m_api{api}
    , m_scheduler{sched}
    , m_read_cb{read_cb}
  {
  }


  inline ~client_impl()
  {
    // Create a list of all current connectors, close them. This can be
    // mildly expensive, it doesn't happen too often.
    std::vector<connector> conns;
    for (auto [_, conn] : m_connected) {
      conns.push_back(conn);
    }
    close_multiple(conns, false);
  }


  inline error_t
  connect_to(liberate::net::url const & remote_url,
      liberate::net::url const & local_url /* = {} */)
  {
    auto [err, result] = resolve_urls(m_api->resolver(), remote_url, local_url);
    if (ERR_SUCCESS != err) {
      return err;
    }

    if (result.empty()) {
      LIBLOG_ERROR("Client needs at least one resolution match; use different "
          "input URLs!");
      return ERR_INVALID_VALUE;
    }

    for (auto [remote, local] : result) {
      auto ret = connect_resolved(remote, local);
      if (ERR_SUCCESS != ret) {
        return ret;
      }
    }

    return ERR_SUCCESS;
  }

  inline error_t
  connect_resolved(liberate::net::url const & remote,
      liberate::net::url const & local)
  {
    error_t err = ERR_SUCCESS;
    auto remote_addr = peer_address{m_api, remote};

    // If there is a local URL, we need to listen on that. This is for datagram
    // oriented connectors, but at this point we only know about URLs.
    connector conn;
    flowid id;
    if (!local.scheme.empty()) {
      conn = connector{m_api, local};

      err = conn.listen();
      if (ERR_SUCCESS != err) {
        return err;
      }

      // If this is not a datagram oriented connector now, we should
      // error out.
      if (!(conn.get_options() & CO_DATAGRAM)) {
        LIBLOG_ERROR("Supplied two URLs, but they're not datagram oriented.");
        return ERR_INVALID_VALUE;
      }

      // Create an identifier for this flow
      id = flowid{peer_address{m_api, local}, remote_addr};
    }
    else {
      // If we did not have a local connector, we must assume stream
      // orientation.
      conn = connector{m_api, remote};

      err = conn.connect();
      if (ERR_SUCCESS != err && ERR_ASYNC != err) {
        return err;
      }

      // If this is now not stream oriented, we error out.
      if (!(conn.get_options() & CO_STREAM)) {
        LIBLOG_ERROR("Supplied one URL, but it's not stream oriented.");
        return ERR_INVALID_VALUE;
      }

      // Create an identifier for this flow
      id = flowid{conn.peer_addr(), remote_addr};
    }

    // This seems good for now; register callbacks.
    if (!conn.communicating()) {
      LIBLOG_ERROR("Cannot initiate communication.");
      return ERR_INVALID_VALUE;
    }

    // Register a close callback.
    callback onclose = [this, id](time_point const & time, events_t const &,
        connector * iconn) -> error_t
    {
      // No longer interested in read events.
      m_scheduler.unregister_connector(PEV_IO_READ, *iconn);

      if (m_close_cb) {
        return m_close_cb(time, id, *iconn);
      }
      return ERR_SUCCESS;
    };
    m_scheduler.register_connector(PEV_IO_CLOSE, conn, onclose,
        IO_FLAGS_ONESHOT);

    // We also want to register a read callback.
    m_scheduler.register_connector(PEV_IO_READ, conn,
        create_read_callback(m_read_cb));

    // All good
    m_connected[remote_addr] = conn;

    // If there is a remote callback, call it now.
    if (m_remote_cb) {
      return m_remote_cb(clock::now(), id, conn);
    }
    return ERR_SUCCESS;
  }


  inline error_t close(connector conn)
  {
    return close_multiple(std::initializer_list<connector>{conn});
  }


  template <typename containerT>
  inline error_t
  close_multiple(containerT const & conns, bool with_callback = true)
  {
    // Unregister the connectors
    for (auto conn : conns) {
      m_scheduler.unregister_connector(conn);
    }
    try {
      m_scheduler.commit_callbacks();
    } catch (...) {
      // Doesn't really matter; we're *unregistering* after all.
      LIBLOG_DEBUG("Failed to unregister connector during shutdown.");
    }

    // Erase all
    connector_map to_erase;
    for (auto conn : conns) {
      for (auto iter = m_connected.begin() ; iter != m_connected.end()
          ; ++iter)
      {
        if (iter->second == conn) {
          to_erase[iter->first] = conn;
        }
      }
    }
    for (auto [remote, _] : to_erase) {
      m_connected.erase(remote);
    }

    // Close callbacks
    if (with_callback && m_close_cb) {
      auto now = clock::now();
      for (auto [remote, conn] : to_erase) {
        auto id = flowid{conn.peer_addr(), remote};
        m_close_cb(now, id, conn);
      }
    }

    // Close all
    error_t ret = ERR_SUCCESS;
    for (auto [_, conn] : to_erase) {
      LIBLOG_DEBUG("Client closing: " << conn);
      auto iret = conn.close();
      if (ERR_SUCCESS == ret && ERR_SUCCESS != iret) {
        // First error is reported!
        ret = iret;
      }
    }

    return ret;
  }
};


basic_client::basic_client(std::shared_ptr<api> api, scheduler & sched,
    simple_callback read_cb)
  : m_impl{std::make_shared<basic_client::client_impl>(
      api, sched, erase_timepoint(read_cb)
    )}
{
}


basic_client::basic_client(std::shared_ptr<api> api, scheduler & sched,
    timepoint_callback read_cb)
  : m_impl{std::make_shared<basic_client::client_impl>(
      api, sched, read_cb
    )}
{
}


basic_client &
basic_client::on_remote(simple_flowid_callback remote_cb)
{
  m_impl->m_remote_cb = erase_timepoint(remote_cb);
  return *this;
}


basic_client &
basic_client::on_remote(timepoint_flowid_callback remote_cb)
{
  m_impl->m_remote_cb = remote_cb;
  return *this;
}


basic_client &
basic_client::on_close(simple_flowid_callback close_cb)
{
  m_impl->m_close_cb = erase_timepoint(close_cb);
  return *this;
}


basic_client &
basic_client::on_close(timepoint_flowid_callback close_cb)
{
  m_impl->m_close_cb = close_cb;
  return *this;
}


error_t
basic_client::connect_to(liberate::net::url const & remote_url,
    liberate::net::url const & local_url /* = {} */)
{
  return m_impl->connect_to(remote_url, local_url);
}


basic_client &
basic_client::connect(liberate::net::url const & remote_url,
    liberate::net::url const & local_url /* = {} */)
{
  auto err = connect_to(remote_url, local_url);
  if (ERR_SUCCESS != err) {
    throw exception(err);
  }
  return *this;
}


bool
basic_client::is_connected() const
{
  // cppcheck-suppress unusedVariable
  for (auto & [_, conn] : m_impl->m_connected) {
    if (conn.communicating()) {
      return true;
    }
  }
  return false;
}


error_t
basic_client::close(connector conn)
{
  return m_impl->close(conn);
}


basic_client::connector_map const &
basic_client::connected() const
{
  return m_impl->m_connected;
}



} // namespace packeteer::util
