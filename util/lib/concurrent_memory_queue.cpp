/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <packeteer/util/io_queue/concurrent_memory_queue.h>

#include <liberate/concurrency/concurrent_queue.h>

#include <vector>
#include <cstring>

namespace packeteer::util {

struct concurrent_memory_queue::cmq_impl
{
  // We don't really do much in terms of implementation. We enqueue data chunks,
  // which we have to copy (an alternative would be just a concurrent_queue with
  // packet pointers). So for packet data, we use a vector. However, as the
  // concurrent_queue also copies entries, we actually do pass pointers to
  // entries.
  using entry_type = std::vector<liberate::types::byte>;
  using entry_ptr = std::shared_ptr<entry_type>;

  // The main queue is simply a concurrent queue of entry type.
  using queue_type = liberate::concurrency::concurrent_queue<entry_ptr>;
  queue_type m_queue = {};

  // For push_front(), we simply use a second queue. Without race conditions,
  // this does not re-order queue entries. But this cannot be guaranteed with
  // multiple threads racing.
  queue_type m_front_queue = {};


  inline bool empty() const
  {
    return m_queue.empty() && m_front_queue.empty();
  }


  inline ssize_t size() const
  {
    return m_queue.size() + m_front_queue.size();
  }


  inline bool push_back(liberate::types::byte const * buf, std::size_t bufsize)
  {
    // Pushing back is easy; we use the main queue.
    return push_internal(m_queue, buf, bufsize);
  }


  inline bool push_front(liberate::types::byte const * buf, std::size_t bufsize)
  {
    // Pushing front is similarly easy is easy; we use the front queue.
    return push_internal(m_front_queue, buf, bufsize);
  }


  inline std::size_t pop_front(liberate::types::byte * buf, std::size_t bufsize)
  {
    // Popping is relatively simple as well, but takes some more logic.
    // We first try the front queue, and if that fails, we take the main
    // queue.
    auto ret = pop_internal(m_front_queue, buf, bufsize);
    if (ret > 0) {
      return ret;
    }
    return pop_internal(m_queue, buf, bufsize);
  }




  inline bool push_internal(queue_type & queue,
      liberate::types::byte const * buf, std::size_t bufsize)
  {
    if (!buf || !bufsize) {
      return false;
    }

    // Pushing back is easy; we use the main queue.
    entry_ptr ptr = std::make_shared<entry_type>();
    ptr->resize(bufsize);
    std::memcpy(ptr->data(), buf, bufsize);

    queue.push(ptr);

    return true;
  }


  inline std::size_t pop_internal(queue_type & queue,
      liberate::types::byte * buf, std::size_t bufsize)
  {
    if (!buf || !bufsize) {
      return 0;
    }

    // Popping an entry is simple enough.
    entry_ptr ptr;
    if (!queue.pop(ptr)) {
      return 0;
    }

    // The tricky part is that we now have an entry from the queue, but the
    // provided buffer may be too small to hold it.
    if (ptr->size() > bufsize) {
      // What can we do here, realistically? The answer is to put the entry
      // back. But the only place we can put it back to is the front queue,
      // which can mess up the order. Well, so be it.
      m_front_queue.push(ptr);
      return 0;
    }

    // If we have enough buffer, let's copy the entry over and be done
    // with it.
    std::memcpy(buf, ptr->data(), ptr->size());
    return ptr->size();
  }
};



concurrent_memory_queue::concurrent_memory_queue()
  : m_impl{std::make_shared<cmq_impl>()}
{
}



bool
concurrent_memory_queue::empty() const
{
  return m_impl->empty();
}


ssize_t
concurrent_memory_queue::size() const
{
  return m_impl->size();
}



bool
concurrent_memory_queue::push_back(liberate::types::byte const * buf,
    std::size_t bufsize)
{
  return m_impl->push_back(buf, bufsize);
}



std::size_t
concurrent_memory_queue::pop_front(liberate::types::byte * buf,
    std::size_t bufsize)
{
  return m_impl->pop_front(buf, bufsize);
}



bool
concurrent_memory_queue::push_front(liberate::types::byte const * buf,
    std::size_t bufsize)
{
  return m_impl->push_front(buf, bufsize);
}

} // namespace packeteer::util
