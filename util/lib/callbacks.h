/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_LIB_CALLBACKS_H
#define PACKETEER_UTIL_LIB_CALLBACKS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

namespace packeteer::util {

/**
 * The functions in this file create callbacks for datagram, stream and any
 * type of connector, each forwarding the main implementation to a simple
 * callback type.
 *
 * The callback type is templated, but expects a time_point and a connector
 * (not by pointer!)
 */
template <typename forwardT>
inline callback
create_write_callback(forwardT forward)
{
  return [forward](time_point const & time, events_t const & events, connector * conn) -> error_t
  {
    // Ignore all but write events (just in case)
    if (!(events & PEV_IO_WRITE)) {
      return ERR_SUCCESS;
    }

    // We don't actually write anything - we just forward the relevant items.
    return forward(time, *conn);
  };
}


template <typename forwardT>
inline callback
create_read_callback(forwardT forward)
{
  return [forward](time_point const & time, events_t const & events, connector * conn) -> error_t
  {
    // Ignore all but read events (just in case)
    if (!(events & PEV_IO_READ)) {
      return ERR_SUCCESS;
    }

    // We don't actually read anything - we just forward the relevant items.
    return forward(time, *conn);
  };
}


template <
  typename forwardT,
  typename flowid_forwardT,
  typename location_forwardT
>
inline callback
create_accept_callback(scheduler & scheduler, forwardT read_forward,
    flowid_forwardT remote_forward,
    location_forwardT close_forward)
{
  return [=, &scheduler](time_point const & time, events_t const & events, connector * conn) mutable -> error_t
  {
    // Ignore all but read events (just in case)
    if (!(events & PEV_IO_READ)) {
      return ERR_SUCCESS;
    }

    // Accepting a new connection should be simple, but unfortunately it's not
    // In asynchronous I/O, the notification that an accept() call should be
    // issued may be sent multiple times before the accept() call finishes, even
    // when only one connection is incoming. We therefore have to be lenient
    // about error handling, here.
    try {
      auto new_conn = conn->accept();

      // This should make the new connection communicating.
      if (new_conn.communicating()) {
        // Great, let's set up read and write connectors for the new connection,
        // as well as a close callback.
        auto read_cb = create_read_callback(read_forward);

        scheduler.register_connector(PEV_IO_READ, new_conn, read_cb);

        flowid id{conn->peer_addr(), new_conn.peer_addr()};
        callback onclose = [id, close_forward](time_point const & t, events_t const &, connector * c) -> error_t
        {
          return close_forward(t, id, *c);
        };
        scheduler.register_connector(PEV_IO_CLOSE, new_conn, onclose);

        scheduler.commit_callbacks();

        // We should call the remote forward as well; this is the moment it's
        // designed for.
        error_t err = ERR_SUCCESS;
        if (remote_forward) {
          err = remote_forward(time, id, new_conn);
          if (ERR_SUCCESS != err) {
            return err;
          }
        }

        // Try to forward to the read callback at least once; there may already
        // be pending data.
        err = read_cb(time, events, &new_conn);
        if (ERR_ASYNC != err) {
          return err;
        }

        // Fall through to ERR_SUCCESS
      }
    } catch (exception const & ex) {
      if (ERR_REPEAT_ACTION == ex.code()) {
        return ERR_SUCCESS;
      }
      return ex.code();
    }

    return ERR_SUCCESS;
  };
}


} // namespace packeteer::util

#endif // guard
