/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/flow.h>

#include <liberate/logging.h>

#include "flow_impl.h"

namespace packeteer::util {


flow::flow(std::unique_ptr<flow::flow_impl> impl)
{
  auto ptr = impl.release();
  if (!ptr) {
    throw exception{ERR_INVALID_VALUE,
      "A null implementation is not permitted!"};
  }

  m_impl = std::shared_ptr<flow_impl>{ptr};
}



error_t
flow::read(void * buf, size_t bufsize, size_t & bytes_read)
{
  return m_impl->read(buf, bufsize, bytes_read);
}



error_t
flow::write(void const * buf, size_t bufsize, size_t & bytes_written)
{
  return m_impl->write(buf, bufsize, bytes_written);
}



flowid
flow::id() const
{
  return m_impl->m_id;
}



packeteer::connector
flow::conn() const
{
  return m_impl->m_conn;
}



error_t
flow::close()
{
  return m_impl->close();
}



bool
flow::communicating() const
{
  return m_impl->m_conn.communicating();
}



size_t
flow::peek() const
{
  return m_impl->m_conn.peek();
}


} // namespace packeteer::util
