/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_LIB_WRITER_H
#define PACKETEER_UTIL_LIB_WRITER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <functional>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <packeteer/util/callbacks.h>

#include "callbacks.h"

namespace packeteer::util {

/**
 * This is a helper struct for managing write callbacks.
 *
 * I/O does not work in a particularly balanced way. On the read side, an
 * event is only raised when there is data in the buffer. The correct way to
 * deal with this is to register a read callback, and then read until there is
 * no more data. If you can't read all available data, it's also ok to wait
 * for the next callback invocation or some such. Read callbacks will be
 * invoked until the OS buffer is drained.
 *
 * On the write side, ostensibly it's the same. In practice, however, write
 * callbacks will be invoked much more often - namely whenever there is even
 * the slightest amount of write buffer available to the OS. As long as you're
 * processing to produce write data, i.e. not just mindlessly piping stuff to
 * a socket, the buffer will likely be drained and the write callback will be
 * invoked.
 *
 * No problem, right?
 *
 * Well, what that means is that the thread waiting for write events will not
 * wait long. If there is (almost) always a write event to schedule, waiting
 * will be over immediately.
 *
 * The correct way for balancing such interrupts is for an application to
 * attempt to write first, and fall back to a write callback *only* if the write
 * failed due to a full buffer. The callback must be a one-shot, write what it
 * can, and then exit. It must be up to the application to repeat the process.
 *
 * To put it differently, read must largely be event-based. Write must largely
 * be push-based, and the event helps optimize this a little.
 */
struct writer
{
  inline writer(scheduler & _scheduler, timepoint_callback write_cb)
    : m_scheduler{_scheduler}
    , m_write_cb{write_cb}
  {
  }

  inline writer(scheduler & _scheduler, simple_callback write_cb)
    : m_scheduler{_scheduler}
    , m_write_cb{erase_timepoint(write_cb)}
  {
  }


  /**
   * Generic wrappers for write() and send().
   */
  inline error_t
  write(connector conn, void const * buffer, size_t bufsize,
      size_t & written)
  {
    return do_write(conn, buffer, bufsize, written,
        [conn](void const * buf, size_t size, size_t & wrt) mutable -> error_t
        {
          return conn.write(buf, size, wrt);
        }
    );
  }

  inline error_t
  send(connector conn, void const * buffer, size_t bufsize, size_t & written,
      peer_address const & recipient)
  {
    return do_write(conn, buffer, bufsize, written,
        [conn, recipient](void const * buf, size_t size, size_t & wrt) mutable -> error_t
        {
          return conn.send(buf, size, wrt, recipient);
        }
    );
  }

  inline error_t
  send(connector conn, void const * buffer, size_t bufsize, size_t & written,
      ::liberate::net::socket_address const & recipient)
  {
    return do_write(conn, buffer, bufsize, written,
        [conn, recipient](void const * buf, size_t size, size_t & wrt) mutable -> error_t
        {
          return conn.send(buf, size, wrt, recipient);
        }
    );
  }


  inline void
  wait_for_writable(connector conn)
  {
    m_scheduler.register_connector(PEV_IO_WRITE, conn,
        create_write_callback(m_write_cb),
        IO_FLAGS_ONESHOT);
    m_scheduler.commit_callbacks();
  }

private:

  using write_func = std::function<error_t (void const *, size_t, size_t &)>;

  inline error_t
  do_write(connector conn, void const * buffer, size_t bufsize, size_t & written,
      write_func callback)
  {
    // The first thing we have to do is just try and write the given data.
    auto err = callback(buffer, bufsize, written);

    switch (err) {
      case ERR_SUCCESS:
        // Great, we're done!
        return ERR_SUCCESS;

      case ERR_REPEAT_ACTION:
        // Couldn't write right now! Let's try later when the connector is
        // more likely to be available. Have it invoked just *once*, though,
        // because that is enough.
        wait_for_writable(conn);

        // Still return ERR_REPEAT_ACTION that the caller shouldn't discard
        // the buffer just yet.
        return ERR_REPEAT_ACTION;

      default:
        return err;
    }
  }

  scheduler &         m_scheduler;
  timepoint_callback  m_write_cb;
};



} // namespace packeteer::util

#endif // guard
