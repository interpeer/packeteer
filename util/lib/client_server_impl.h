/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <build-config.h>

#include <mutex>

#include <liberate/logging.h>

#include <packeteer/util/flow.h>

#define PACKETEER_UTIL_USE_DETAIL
#include <packeteer/util/detail/flow_manager.h>
#undef PACKETEER_UTIL_USE_DETAIL

#include "flow_impl.h"

namespace packeteer::util {

/**
 * Shared code between server::server_impl and client::client_impl, which it
 * turns out is a fair amount. We abstract out the basic_client vs.
 * basic_server types via basicT.
 *
 * The other template parameter is for the derived class. Only it has access
 * to flow's private members via the friend declaration, so we have to move
 * some very small functions back into the dervied class.
 */
template <
  typename basicT,
  typename implT
>
struct client_server_impl
{
  std::shared_ptr<api>        m_api;
  scheduler &                 m_scheduler;

  // Flow_manager must appear before basicT, because the destructor of
  // basicT must run before flow_manager's
  detail::flow_manager        m_fm;
  basicT                      m_basic;

  timepoint_flow_callback     m_remote_cb = {};
  timepoint_flow_callback     m_close_cb = {};
  timepoint_flow_callback     m_read_cb;

  io_queue_creator            m_io_queue_creator;

  flow_map                    m_flows = {};

  std::mutex                  m_mutex;

  inline client_server_impl(std::shared_ptr<api> api,
      scheduler & sched, duration timeout,
      timepoint_flow_callback read_cb,
      io_queue_creator queue_creator
  )
    : m_api{api}
    , m_scheduler{sched}
    , m_fm{m_scheduler, timeout}
    , m_basic{
        m_api, m_scheduler,
        timepoint_callback{std::bind(
          &client_server_impl::on_readable, this,
          std::placeholders::_1,
          std::placeholders::_2
        )}
      }
    , m_read_cb{read_cb}
    , m_io_queue_creator{queue_creator}
  {
    using namespace std::placeholders;

    m_fm.on_remote([this](flowid const & id, connector conn) -> error_t
      {
        LIBLOG_DEBUG("Remote established: " << id);
        // Create a flow with this identifier.
        auto ptr = as_derived()->make_impl(id, conn, m_scheduler,
            m_read_cb, m_io_queue_creator,
            flow_impl_close_func{
              std::bind(&client_server_impl::close, this, std::placeholders::_1)
            }
        );

        auto [iter, success] = as_derived()->add_flow(id, std::move(ptr));
        if (!success) {
          LIBLOG_ERROR("Could not insert new flow; this may be an issue in "
              "flow manager.");
          return ERR_UNEXPECTED;
        }

        // If we have no remote callback, we're done. Otherwise, we'll have to
        // invoke it.
        if (!m_remote_cb) {
          return ERR_SUCCESS;
        }

        return m_remote_cb(clock::now(), iter->second);
      }
    );
    m_fm.on_close([this](flowid const & id, connector) -> error_t
      {
        std::unique_lock<std::mutex> lock{m_mutex};

        close_impl(id, lock);
        // Ignore return value, always success
        return ERR_SUCCESS;
      }
    );

    m_basic.on_remote(timepoint_flowid_callback{
        std::bind(&detail::flow_manager::remote_callback, &m_fm, _1, _2, _3)
    });
    m_basic.on_close(timepoint_flowid_callback{
        std::bind(&detail::flow_manager::close_callback, &m_fm, _1, _2, _3)
    });
  }



  inline ~client_server_impl()
  {
    // Need to put IDs into a set, because we can't call close() while
    // iterating the m_flows map.
    std::set<flowid> ids;
    std::unique_lock<std::mutex> lock{m_mutex};
    for (auto & [id, _] : m_flows) {
      ids.insert(id);
    }

    close_impl(ids, lock);
  }



  inline error_t
  on_readable(time_point const & time, connector conn)
  {
    // Distinguish between stream and datagram versions of this.
    if (conn.get_options() & CO_DATAGRAM) {
      return on_readable_dgram(time, conn);
    }
    return on_readable_stream(time, conn);
  }



  inline implT *
  as_derived()
  {
    return reinterpret_cast<implT *>(this);
  }



  inline error_t
  on_readable_dgram(time_point const & time, connector conn)
  {
    // With datagrams, there is no choice but to read. But this is fine. It just
    // means we *first* read, and *then* decide to which flow the data belongs.
    //
    // We also need to do this until reading fails - we're draining the OS I/O
    // buffers here. Each read packet may belong to a different flow, though.
    char buf[PACKETEER_IO_BUFFER_SIZE];
    size_t read = 0;
    liberate::net::socket_address sender;
    error_t err = ERR_SUCCESS;

    do {
      err = conn.receive(buf, sizeof(buf), read, sender);
      if (ERR_SUCCESS != err) {
        break;
      }

      // Create a flow id.
      flowid id{
        conn.peer_addr(),
        peer_address{m_api, conn.peer_addr().conn_type(), sender}
      };

      // Add to flow management. Ignore result, we don't need to know here.
      m_fm.add(id, conn, time);

      // We now know about the flow, and can add to the flow's
      // queue.
      if (read > 0) {
        insert_into_read_queue(time, id, buf, read);
      }
    } while (true);

    if (ERR_REPEAT_ACTION == err) {
      // I/O buffers drained
      return ERR_SUCCESS;
    }
    return err;
  }



  inline error_t
  on_readable_stream(time_point const & now, connector conn)
  {
    // For stream connectors, we don't have to do anything about flow
    // management. Also, we use read() instead of receive().

    // For actual *reading* purposes, the connector is enough. But to also
    // identify the correct queues to use, we need to find the flow in our
    // flow map that matches the connector.
    std::unique_lock<std::mutex> lock{m_mutex};

    // cppcheck-suppress unusedVariable
    for (auto & [_, flow] : m_flows) {
      if (conn == flow.conn()) {
        lock.unlock(); // Unlock mutex; conn and flow are copied
        return stream_read_into_queue(now, conn, flow);
      }
    }

    LIBLOG_WARN("Can't find flow associated with connector: " << conn);
    return ERR_SUCCESS; // Ignore
  }


  inline error_t
  stream_read_into_queue(time_point const & now, connector conn, flow the_flow)
  {
    char buf[PACKETEER_IO_BUFFER_SIZE];
    size_t read = 0;
    error_t err = ERR_SUCCESS;

    do {
      err = conn.read(buf, sizeof(buf), read);
      if (ERR_SUCCESS != err) {
        break;
      }

      if (read <= 0) {
        // Nothing to read
        break;
      }

      // We now know about the flow, and can add to the flow's
      // queue.
      as_derived()->add_to_read_queue(now, the_flow, buf, read);
    } while (true);

    if (ERR_REPEAT_ACTION == err) {
      // I/O buffers drained
      return ERR_SUCCESS;
    }
    return err;
  }


  inline void
  insert_into_read_queue(time_point const & now, flowid const & id,
      char const * buf, size_t amount)
  {
    LIBLOG_DEBUG("Adding " << amount << " Bytes to read queue for " << id);

    std::unique_lock<std::mutex> lock{m_mutex};
    auto iter = m_flows.find(id);
    if (iter == m_flows.end()) {
      LIBLOG_ERROR("Flow manager screwed up; we have to have a flow!");
      return;
    }

    auto & flow = iter->second;

    // Add to flow's read queue
    as_derived()->add_to_read_queue(now, flow, buf, amount);
  }


  inline error_t
  close(flowid const & id)
  {
    std::unique_lock<std::mutex> lock{m_mutex};

    return close_impl(id, lock);
  }


private:
  inline error_t
  close_impl(std::set<flowid> const & ids, std::unique_lock<std::mutex> & lock)
  {
    // First, find the ids in the flow map. We want to remove those flows from
    // the map.
    std::set<flow> matching_flows;
    for (auto id : ids) {
      auto iter = m_flows.find(id);
      if (iter == m_flows.end()) {
        // Not found; ignoring.
        continue;
      }
      auto the_flow = iter->second;
      m_flows.erase(iter);
      matching_flows.insert(the_flow);
    }

    // For each matching flow, we have to determine if its connector is part
    // of any remaining flow.
    std::set<packeteer::connector> to_close;
    // cppcheck-suppress unusedVariable
    for (auto & the_flow : matching_flows) {
      bool found = false;
      for (auto & [_, candidate] : m_flows) {
        if (the_flow.conn() == candidate.conn()) {
          found = true;
          break;
        }
      }

      if (!found) {
        to_close.insert(the_flow.conn());
      }
    }

    // We've done our bookkeeping, unlock.
    lock.unlock();

    // If we have a close callback, invoke it for matching flows.
    if (m_close_cb) {
      auto now = clock::now();
      for (auto & the_flow : matching_flows) {
        m_close_cb(now, the_flow);
      }
    }

    // Now close all connectors we need to with the basic part.
    for (auto conn : to_close) {
      m_scheduler.unregister_connector(conn);
      m_basic.close(conn);
    }
    try {
      m_scheduler.commit_callbacks();
    } catch (packeteer::exception const & ex) {
      LIBLOG_EXC(ex, "Error committing unregistration, ignoring.");
    } catch (std::exception const & ex) {
      LIBLOG_EXC(ex, "Error committing unregistration, ignoring.");
    } catch (...) {
      LIBLOG_ERROR("Unknown error committing unregistration, ignoring.");
    }

    return ERR_SUCCESS;
  }


  inline error_t
  close_impl(flowid const & id, std::unique_lock<std::mutex> & lock)
  {
    std::set<flowid> ids;
    ids.insert(id);
    return close_impl(ids, lock);
  }
};

} // namespace packeteer::util
