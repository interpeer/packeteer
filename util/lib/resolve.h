/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_LIB_RESOLVE_H
#define PACKETEER_UTIL_LIB_RESOLVE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <set>

#include <liberate/logging.h>

#include <packeteer/resolver.h>

namespace packeteer::util {

/**
 * This function is a simple wrapper for resolving URLs via packeteer's
 * resolver; the main functionality it adds is to ensure that given two
 * input URLs, they resolve to *compatible tuples*.
 *
 * - If an input URL cannot be resolved, that is largely ignored; it is
 *   treated as already resolved.
 * - From the sets of results for both input URLs, matching pairs are
 *   created; a matching pair is one where the URL schemes match.
 *
 * For e.g. host names that resolve to IPv4 and IPv6 addresses, this
 * ensures that tuples for IPv4 and tuples for IPv6 are returned, but
 * no tuple contains one IPv4 and one IPv6 address.
 */
using url_tuple = std::tuple<liberate::net::url, liberate::net::url>;
using url_tuple_set = std::set<url_tuple>;
using resolve_result = std::tuple<error_t, url_tuple_set>;

inline resolve_result
resolve_urls(resolver & resolver,
    liberate::net::url const & url1,
    liberate::net::url const & url2 = {})
{
  if (url2.scheme.empty()) {
    LIBLOG_DEBUG("Asked to resolve " << url1);
  }
  else {
    LIBLOG_DEBUG("Asked to match " << url1 << " with " << url2);
  }

  // Resolve url1
  std::set<liberate::net::url> url1_results;
  auto err = resolver.resolve(url1_results, url1);
  if (ERR_INVALID_VALUE == err) {
    url1_results.insert(url1);
  }
  else if (ERR_SUCCESS != err) {
    return {err, {}};
  }

  url_tuple_set results;

  // If there is no url2, we can short-circuit here.
  if (url2.scheme.empty()) {
    for (auto u1 : url1_results) {
      LIBLOG_DEBUG("Resolved to " << u1);
      results.insert(std::make_tuple<liberate::net::url, liberate::net::url>(std::move(u1), {}));
    }
    return {ERR_SUCCESS, results};
  }

  // Otherwise we need to resolve the url2 url
  std::set<liberate::net::url> url2_results;
  err = resolver.resolve(url2_results, url2);
  if (ERR_INVALID_VALUE == err) {
    url2_results.insert(url2);
  }
  else if (ERR_SUCCESS != err) {
    return {err, {}};
  }

  // With both sets resolved, we need to find sets of matching schemes.
  for (auto u1 : url1_results) {
    for (auto u2 : url2_results) {
      if (u1.scheme != u2.scheme) {
        continue;
      }

      // Found a match!
      results.insert(std::make_tuple(u1, u2));
    }
  }

  // We now have a url1 and url2
  LIBLOG_DEBUG("Resolved to " << results.size() << " matching tuples.");

  return {ERR_SUCCESS, results};
}

} // namespace packeteer::util

#endif // guard
