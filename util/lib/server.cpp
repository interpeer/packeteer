/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <build-config.h>

#include <packeteer/util/server.h>
#include <packeteer/util/basic_server.h>

#include "flow_impl.h"
#include "client_server_impl.h"

namespace packeteer::util {


struct server::server_impl
  : public client_server_impl<basic_server, server::server_impl>
{
  inline server_impl(std::shared_ptr<api> api,
      scheduler & sched, duration timeout,
      timepoint_flow_callback read_cb,
      io_queue_creator queue_creator
    )
    : client_server_impl<basic_server, server::server_impl>{
        api, sched, timeout, read_cb, queue_creator
      }
  {
  }


  inline ~server_impl() = default;


  inline std::tuple<flow_map::iterator, bool>
  add_flow(flowid const & id, std::unique_ptr<flow::flow_impl> impl)
  {
    return m_flows.insert({id, std::move(impl)});
  }



  inline void add_to_read_queue(time_point const & now, flow & the_flow,
      char const * buf, size_t amount)
  {
    the_flow.m_impl->add_to_read_queue(now, the_flow, buf, amount);
  }


  inline std::unique_ptr<flow::flow_impl> make_impl(flowid const & id,
      connector conn,
      scheduler & sched,
      timepoint_flow_callback read_cb,
      io_queue_creator queue_creator,
      flow_impl_close_func closer)
  {
    return std::make_unique<flow::flow_impl>(id, conn, sched,
            read_cb,
            queue_creator,
            closer
        );
  }


  inline error_t
  listen_on(liberate::net::url const & conn_url)
  {
    LIBLOG_DEBUG("Asked to listen on: " << conn_url);
    return m_basic.listen_on(conn_url);
  }
};



server::server(std::shared_ptr<api> api, scheduler & sched,
    duration timeout, timepoint_flow_callback read_cb,
    io_queue_creator queue_creator
  )
  : m_impl{std::make_shared<server::server_impl>(
      api, sched, timeout, read_cb,
      queue_creator
    )}
{
}


server &
server::on_remote(simple_flow_callback remote_cb)
{
  m_impl->m_remote_cb = erase_timepoint(remote_cb);
  return *this;
}


server &
server::on_remote(timepoint_flow_callback remote_cb)
{
  m_impl->m_remote_cb = remote_cb;
  return *this;
}


server &
server::on_close(simple_flow_callback close_cb)
{
  m_impl->m_close_cb = erase_timepoint(close_cb);
  return *this;
}


server &
server::on_close(timepoint_flow_callback close_cb)
{
  m_impl->m_close_cb = close_cb;
  return *this;
}


error_t
server::listen_on(liberate::net::url const & conn_url)
{
  return m_impl->listen_on(conn_url);
}


server &
server::listen(liberate::net::url const & conn_url)
{
  auto err = listen_on(conn_url);
  if (ERR_SUCCESS != err) {
    throw exception(err);
  }
  return *this;
}



std::set<connector> const &
server::listening() const
{
  return m_impl->m_basic.listening();
}



std::size_t
server::num_listening() const
{
  return m_impl->m_basic.num_listening();
}



flow_map const &
server::established() const
{
  return m_impl->m_flows;
}



std::size_t
server::num_established() const
{
  return m_impl->m_flows.size();
}



error_t
server::close(flowid const & id)
{
  return m_impl->close(id);
}



error_t
server::close(flow const & the_flow)
{
  return m_impl->close(the_flow.id());
}


} // namespace packeteer::util
