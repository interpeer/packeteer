/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/io_queue/concurrent_memory_queue.h>

#include <gtest/gtest.h>

namespace p7r = packeteer;
namespace pu = packeteer::util;


TEST(PacketeerUtilConcurrentMemoryQueue, simple)
{
  pu::concurrent_memory_queue queue;
  ASSERT_TRUE(queue.empty());
  ASSERT_EQ(0, queue.size());

  // Push
  std::string test{"Hello, world!"};
  auto res = queue.push_back(test.c_str(), test.size());
  ASSERT_TRUE(res);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(1, queue.size());

  // Pop
  char buf[200] = {};
  auto size = queue.pop_front(buf, sizeof(buf));
  ASSERT_EQ(size, test.size());
  ASSERT_TRUE(queue.empty());
  ASSERT_EQ(0, queue.size());

  std::string converted{buf, buf + size};
  ASSERT_EQ(converted, test);
}



TEST(PacketeerUtilConcurrentMemoryQueue, small_pop_buffer)
{
  // Note: a failing pop_back() results in an internal push_front(), so this
  // test also covers push_front() functionality.

  pu::concurrent_memory_queue queue;
  ASSERT_TRUE(queue.empty());
  ASSERT_EQ(0, queue.size());

  // Push
  std::string test{"Hello, world!"};
  auto res = queue.push_back(test.c_str(), test.size());
  ASSERT_TRUE(res);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(1, queue.size());

  // Pop - buffer too small (or rather the size)
  char buf[200] = {};
  auto size = queue.pop_front(buf, 10);
  ASSERT_EQ(size, 0);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(1, queue.size());

  // Pop - again, with right size buffer
  size = queue.pop_front(buf, sizeof(buf));
  ASSERT_EQ(size, test.size());
  ASSERT_TRUE(queue.empty());
  ASSERT_EQ(0, queue.size());

  std::string converted{buf, buf + size};
  ASSERT_EQ(converted, test);
}


TEST(PacketeerUtilConcurrentMemoryQueue, small_pop_buffer_ordering)
{
  // Note: a failing pop_back() results in an internal push_front(), so this
  // test also covers push_front() functionality.

  pu::concurrent_memory_queue queue;
  ASSERT_TRUE(queue.empty());
  ASSERT_EQ(0, queue.size());

  // Push, twice.
  std::string test1{"Hello, test1!"};
  auto res = queue.push_back(test1.c_str(), test1.size());
  ASSERT_TRUE(res);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(1, queue.size());

  std::string test2{"Hello, test2!"};
  res = queue.push_back(test2.c_str(), test2.size());
  ASSERT_TRUE(res);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(2, queue.size());

  // Pop - buffer too small (or rather the size)
  // Do this also twice
  char buf[200] = {};
  auto size = queue.pop_front(buf, 10);
  ASSERT_EQ(size, 0);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(2, queue.size());

  size = queue.pop_front(buf, 10);
  ASSERT_EQ(size, 0);
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(2, queue.size());

  // Pop - again, with right size buffer
  size = queue.pop_front(buf, sizeof(buf));
  ASSERT_EQ(size, test1.size());
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(1, queue.size());

  // The order is preserved - but this is because there is no concurrent
  // access.
  std::string converted{buf, buf + size};
  ASSERT_EQ(converted, test1);
}
