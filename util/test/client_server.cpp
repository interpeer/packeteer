/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/server.h>
#include <packeteer/util/client.h>

#include <liberate/logging.h>

#include <gtest/gtest.h>

namespace p7r = packeteer;
namespace pu = packeteer::util;

namespace {

static constexpr std::size_t BUFSIZE = 8192;

p7r::error_t
echo_callback(pu::flow the_flow)
{
  // Read
  char buf[BUFSIZE];
  size_t read = 0;
  auto err = the_flow.read(buf, sizeof(buf), read);
  if (err != p7r::ERR_SUCCESS) {
    return err;
  }

  // Output
  std::string msg{buf, buf + read};
  LIBLOG_DEBUG("Received from " << the_flow.id() << ": " << msg);

  // Echo right back.
  size_t written = 0;
  err = the_flow.write(buf, read, written);
  if (err == p7r::ERR_ASYNC) {
    return p7r::ERR_SUCCESS;
  }
  return err;
}



p7r::error_t
empty_flow_callback(p7r::util::flow)
{
  return p7r::ERR_SUCCESS;
}


using namespace std::literals::chrono_literals;
auto constexpr TIMEOUT = 50ms;


} // anonymous namespace



TEST(PacketeerUtilClientServer, server_creation)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // We'll register empty callbacks, but we will call all registration
  // functions, if only to exercise the code.
  auto srv = pu::server{
      api,
      scheduler,
      TIMEOUT,
      empty_flow_callback
    }
    .on_close(empty_flow_callback)
    .on_remote(empty_flow_callback)
    .listen("udp://localhost:54321")
  ;

  // XXX We can't know whether localhost resolves to 1 or 2 addresses
  //     (IPv4/IPv6), so we have to be a bit vague here.
  auto listening = srv.num_listening();
  ASSERT_TRUE(1 <= listening && listening <= 2);

  // Established connections should still be zero.
  ASSERT_EQ(0, srv.num_established());
}


TEST(PacketeerUtilClientServer, client_creation)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // We'll register empty callbacks, but we will call all registration
  // functions, if only to exercise the code.
  auto client = pu::client{
      api,
      scheduler,
      TIMEOUT,
      empty_flow_callback
    }
    .on_close(empty_flow_callback)
    .on_remote(empty_flow_callback)
  ;

  // We're not connecting anywhere without a server running, so assert that the
  // client is not connected.
  ASSERT_FALSE(client.is_connected());

  // Established connections should still be zero.
  ASSERT_EQ(0, client.num_established());
}



TEST(PacketeerUtilClientServer, udp_echo)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // Create a server with an echo callback
  auto srv = pu::server{
      api,
      scheduler,
      TIMEOUT,
      echo_callback
    }
    .listen("udp4://localhost:54321")
  ;

  std::string test_message = "Hello, echo!";

  // Create a client. We need a local address as well for UDP.
  std::string result;

  auto client = pu::client{
      api,
      scheduler,
      TIMEOUT,
      [&result](pu::flow the_flow) -> p7r::error_t
      {
        // Let's read, using a temp buffer of appropriate size.
        std::vector<char> buf;
        buf.resize(512);

        // Read from the flow
        size_t have_read = 0;
        auto err = the_flow.read(&buf[0], buf.size(), have_read);
        EXPECT_EQ(p7r::ERR_SUCCESS, err);
        EXPECT_NE(0, have_read);

        buf.resize(have_read);

        // Copy the buffer over to the result.
        result = std::string{buf.begin(), buf.end()};

        return p7r::ERR_SUCCESS;
      }
    }
    .connect("udp://127.0.0.1:54321", "udp://127.0.0.1:54322")
  ;

  // The client should have an established flow here! The implication is
  // that also the server should. But this is not true unless the client
  // has sent a message in UDP situations.
  ASSERT_EQ(1, client.num_established());
  ASSERT_EQ(0, srv.num_established());

  auto [id, client_flow] = *client.established().begin();

  // Technically, this should be enough to send messages from client to server
  // and vice versa. Since the server implements an echo callback, we write on
  // the client connector.
  size_t written = 0;
  auto err = client_flow.write(test_message.c_str(), test_message.size(),
      written);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(test_message.size(), written);


  // Now we have to let the scheduler do its work. We trigger it twice; once for
  // sending, once for receiving.
  using namespace std::chrono_literals;
  scheduler.process_events(20ms);
  scheduler.process_events(20ms);

  // The result should now be identical to the message
  ASSERT_EQ(test_message, result);
}



TEST(PacketeerUtilClientServer, tcp_echo)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // Create a server with an echo callback
  auto srv = pu::server{
      api,
      scheduler,
      TIMEOUT,
      echo_callback
    }
    .listen("tcp4://localhost:54321")
  ;

  std::string test_message = "Hello, echo!";

  // Create a client. We need a local address as well for UDP.
  std::string result;

  auto client = pu::client{
      api,
      scheduler,
      TIMEOUT,
      [&result](pu::flow the_flow) -> p7r::error_t
      {
        // Let's read, using a temp buffer of appropriate size.
        std::vector<char> buf;
        buf.resize(512);

        // Read from the flow
        size_t have_read = 0;
        auto err = the_flow.read(&buf[0], buf.size(), have_read);
        EXPECT_EQ(p7r::ERR_SUCCESS, err);
        EXPECT_NE(0, have_read);

        buf.resize(have_read);

        // Copy the buffer over to the result.
        result = std::string{buf.begin(), buf.end()};

        return p7r::ERR_SUCCESS;

      }
    }
    .connect("tcp://127.0.0.1:54321")
  ;

  // The client should have an established flow here! The implication is
  // that also the server should. But this is not true unless the client
  // has sent a message in UDP situations.
  ASSERT_EQ(1, client.num_established());
  ASSERT_EQ(0, srv.num_established());

  auto [id, client_flow] = *client.established().begin();

  // Technically, this should be enough to send messages from client to server
  // and vice versa. Since the server implements an echo callback, we write on
  // the client connector.
  size_t written = 0;
  auto err = client_flow.write(test_message.c_str(), test_message.size(),
      written);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(test_message.size(), written);


  // Now we have to let the scheduler do its work. We trigger it twice; once for
  // sending, once for receiving.
  using namespace std::chrono_literals;
  scheduler.process_events(20ms);
  scheduler.process_events(20ms);

  // The result should now be identical to the message
  ASSERT_EQ(test_message, result);
}
