/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#define PACKETEER_UTIL_USE_DETAIL 1
#include <packeteer/util/detail/flow_manager.h>

#include <packeteer/util/basic_server.h>
#include <packeteer/util/basic_client.h>

#include <packeteer/scheduler.h>

#include <liberate/logging.h>

#include <gtest/gtest.h>

namespace p7r = packeteer;
namespace pu = packeteer::util;


namespace {

static constexpr std::size_t BUFSIZE = 8192;

p7r::error_t
empty_callback(p7r::connector)
{
  return p7r::ERR_SUCCESS;
}


template <typename fmT>
pu::timepoint_callback
create_dgram_callback(std::shared_ptr<p7r::api> api, fmT & fm)
{
  return [api, &fm](p7r::time_point const & time, p7r::connector conn) -> p7r::error_t
  {
    char buf[BUFSIZE];
    size_t read = 0;
    liberate::net::socket_address sender;
    auto err = conn.receive(buf, sizeof(buf), read, sender);
    if (err != p7r::ERR_SUCCESS) {
      return err;
    }

    // XXX In a real implementation, we'd also want to do something with the received
    //     message, but here it's just about flow management.

    // Add this flow to the flow manager.
    pu::flowid id{
      conn.peer_addr(),
      p7r::peer_address{api, conn.peer_addr().conn_type(), sender}
    };
    LIBLOG_DEBUG("UDP flow id: " << id);
    return fm.add(id, conn, time);
  };
}

} // anonymous namespace


TEST(PacketeerUtilFlowManager, flowid_uniqueness)
{
  // This test case mainly verifies the flow manager's map behaviour.
  auto api = packeteer::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  auto conn = p7r::connector{api, "anon://"};

  pu::detail::flow_manager fm{scheduler};

  int open_count = 0;
  fm.on_remote(
      [&open_count](pu::flowid const &, p7r::connector) -> p7r::error_t
      {
        ++open_count;
        return p7r::ERR_SUCCESS;
      }
  );

  // Listening socket
  ASSERT_EQ(0, open_count);
  pu::flowid id1{conn.peer_addr(), {}};
  ASSERT_TRUE(id1.is_local());
  ASSERT_FALSE(id1.is_remote());
  ASSERT_FALSE(id1.is_full());

  auto err = fm.add(id1, conn);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(1, open_count);

  // Adding the same combination should succeed silently.
  err = fm.add(id1, conn);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(1, open_count); // Still just once

  // Adding the same flowid with a different connection should fail.
  err = fm.add(id1, p7r::connector{api, "anon://"});
  ASSERT_EQ(p7r::ERR_INVALID_VALUE, err);

  // Connected socket (the exact addresses don't matter; what matters is that
  // there are two parts to it)
  pu::flowid id2{conn.peer_addr(), conn.peer_addr()};
  ASSERT_FALSE(id2.is_local());
  ASSERT_FALSE(id2.is_remote());
  ASSERT_TRUE(id2.is_full());

  err = fm.add(id2, conn);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  // Silent success on duplicate
  err = fm.add(id2, conn);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  // Failure on a different connector
  err = fm.add(id2, p7r::connector{api, "anon://"});
  ASSERT_EQ(p7r::ERR_INVALID_VALUE, err);

  // Removal doesn't need a connector.
  err = fm.remove(id1);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  err = fm.remove(id1);
  ASSERT_EQ(p7r::ERR_INVALID_VALUE, err);

  // The flow manager works on flowids, and cannot - should not - distinguish
  // between full and partial flow ids when removing one that is *local*. As such,
  // having removed id1 successfully, also id2 must be gone.
  // err = fm.remove(id2);
  // ASSERT_EQ(p7r::ERR_SUCCESS, err);

  err = fm.remove(id2);
  ASSERT_EQ(p7r::ERR_INVALID_VALUE, err);
}


TEST(PacketeerUtilFlowManager, auto_removal)
{
  // This test case mainly verifies the flow manager's map behaviour.
  auto api = packeteer::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  auto addr = "tcp4://127.0.0.1:54321";
  auto server_conn = p7r::connector{api, addr};

  // Need to create three connectors for close events.
  auto err = server_conn.listen();
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  auto client_conn = p7r::connector{api, addr};
  err = client_conn.connect();
  ASSERT_EQ(p7r::ERR_ASYNC, err);

  auto conn = server_conn.accept();
  ASSERT_TRUE(conn.communicating());

  pu::detail::flow_manager fm{scheduler};

  size_t close_count = 0;
  fm.on_close(
      [&close_count](pu::flowid const &, p7r::connector) -> p7r::error_t
      {
        ++close_count;
        return p7r::ERR_SUCCESS;
      }
  );

  // Listening socket
  pu::flowid id1{conn.peer_addr(), p7r::peer_address{api, addr}};
  err = fm.add(id1, conn);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  // Closing the remote (client) socket should remove it from the flow manager.
  ASSERT_TRUE(fm.contains(id1));

  client_conn.close();

  using namespace std::literals::chrono_literals;
  ASSERT_EQ(0, close_count);
  scheduler.process_events(20ms);

  ASSERT_EQ(1, close_count);
  ASSERT_FALSE(fm.contains(id1));
}


TEST(PacketeerUtilFlowManager, client_server_tcp)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // Create a server with an echo callback
  size_t srv_remote_count = 0;
  size_t srv_close_count = 0;
  auto srv_fm = pu::detail::flow_manager{scheduler}
    .on_remote([&srv_remote_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Server remote: " << id);
          ++srv_remote_count;
          return p7r::ERR_SUCCESS;
        }
    )
    .on_close([&srv_close_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Server close: " << id);
          ++srv_close_count;
          return p7r::ERR_SUCCESS;
        }
    )
  ;

  using namespace std::placeholders;
  auto srv = pu::basic_server{
      api,
      scheduler,
      empty_callback
    }
    .on_remote(pu::timepoint_flowid_callback{std::bind(&pu::detail::flow_manager::remote_callback, &srv_fm, _1, _2, _3)})
    .on_close(pu::timepoint_flowid_callback{std::bind(&pu::detail::flow_manager::close_callback, &srv_fm, _1, _2, _3)})
    .listen("tcp4://localhost:54321")
  ;

  // At this point, the remote and close counts should be zero
  ASSERT_EQ(0, srv_remote_count);
  ASSERT_EQ(0, srv_close_count);

  // Create a client.
  size_t cl_remote_count = 0;
  size_t cl_close_count = 0;
  auto cl_fm = pu::detail::flow_manager{scheduler}
    .on_remote([&cl_remote_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Client remote: " << id);
          ++cl_remote_count;
          return p7r::ERR_SUCCESS;
        }
    )
    .on_close([&cl_close_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Client close: " << id);
          ++cl_close_count;
          return p7r::ERR_SUCCESS;
        }
    )
  ;

  auto client = pu::basic_client{
      api,
      scheduler,
      empty_callback
    }
    .on_remote(pu::timepoint_flowid_callback{std::bind(&pu::detail::flow_manager::remote_callback, &cl_fm, _1, _2, _3)})
    .connect("tcp://127.0.0.1:54321")
  ;

  // Let the scheduler do its work for a bit.
  using namespace std::literals::chrono_literals;
  scheduler.process_events(20ms);

  // The remote callback should have been called on the client and server side.
  ASSERT_EQ(1, srv_remote_count);
  ASSERT_EQ(0, srv_close_count);
  ASSERT_EQ(1, cl_remote_count);
  ASSERT_EQ(0, cl_close_count);

  // Closing the client should invoke the server's close function and vice
  // versa. Since we already closed the client in the auto_removal test, and
  // doing both is hard without opening yet another client, let's just close
  // the server connection and verify the client's callback has been called.
  auto srv_conns = srv.established();
  for (auto conn : srv_conns) {
    srv.close(conn);
  }

  scheduler.process_events(20ms);

  ASSERT_EQ(1, srv_remote_count);
  ASSERT_EQ(1, srv_close_count);
  ASSERT_EQ(1, cl_remote_count);
  ASSERT_EQ(1, cl_close_count);
}


TEST(PacketeerUtilFlowManager, client_server_udp)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  using namespace std::literals::chrono_literals;
  auto constexpr TIMEOUT = 50ms;

  // Create a server with an echo callback
  size_t srv_remote_count = 0;
  size_t srv_close_count = 0;
  auto srv_fm = pu::detail::flow_manager{scheduler, TIMEOUT}
    .on_remote([&srv_remote_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Server remote: " << id);
          ++srv_remote_count;
          return p7r::ERR_SUCCESS;
        }
    )
    .on_close([&srv_close_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Server close: " << id);
          ++srv_close_count;
          return p7r::ERR_SUCCESS;
        }
    )
  ;

  using namespace std::placeholders;
  auto srv = pu::basic_server{
      api,
      scheduler,
      create_dgram_callback(api, srv_fm)
    }
    .on_remote(pu::timepoint_flowid_callback{std::bind(&pu::detail::flow_manager::remote_callback, &srv_fm, _1, _2, _3)})
    .listen("udp4://localhost:54321")
  ;

  // At this point, the remote and close counts should be zero
  ASSERT_EQ(0, srv_remote_count);
  ASSERT_EQ(0, srv_close_count);

  // Create a client.
  size_t cl_remote_count = 0;
  size_t cl_close_count = 0;
  auto cl_fm = pu::detail::flow_manager{scheduler, TIMEOUT}
    .on_remote([&cl_remote_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Client remote: " << id);
          ++cl_remote_count;
          return p7r::ERR_SUCCESS;
        }
    )
    .on_close([&cl_close_count](pu::flowid const & id [[maybe_unused]], p7r::connector) -> p7r::error_t
        {
          LIBLOG_DEBUG("Client close: " << id);
          ++cl_close_count;
          return p7r::ERR_SUCCESS;
        }
    )
  ;

  auto client = pu::basic_client{
      api,
      scheduler,
      create_dgram_callback(api, cl_fm)
    }
    .on_remote(pu::timepoint_flowid_callback{std::bind(&pu::detail::flow_manager::remote_callback, &cl_fm, _1, _2, _3)})
    .connect("udp://127.0.0.1:54321", "udp://127.0.0.1:54322")
  ;

  auto [remote, conn] = *client.connected().begin();

  // We cannot expect UDP servers to receive a remote event without sending a
  // message.
  std::string msg{"Hello, world!"};
  size_t sent = 0;
  auto err = conn.send(msg.c_str(), msg.size(),
      sent,
      remote);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(msg.size(), sent);

  // Let the scheduler do its work for a bit.
  scheduler.process_events(20ms);

  // The remote callback should have been called on the client and server side.
  ASSERT_EQ(1, srv_remote_count);
  ASSERT_EQ(0, srv_close_count);
  ASSERT_EQ(1, cl_remote_count);
  ASSERT_EQ(0, cl_close_count);

  // If we wait for the connection timeouts to expire, then we should have the
  // close callbacks as well.
  scheduler.process_events(TIMEOUT);
  ASSERT_EQ(1, srv_remote_count);
  ASSERT_EQ(1, srv_close_count);
  ASSERT_EQ(1, cl_remote_count);
  ASSERT_EQ(1, cl_close_count);
}
