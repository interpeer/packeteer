/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/util/basic_server.h>
#include <packeteer/util/basic_client.h>

#include <liberate/logging.h>

#include <gtest/gtest.h>

namespace p7r = packeteer;
namespace pu = packeteer::util;

namespace {

static constexpr std::size_t BUFSIZE = 8192;

p7r::error_t
echo_callback_stream(p7r::connector conn)
{
  // Read
  char buf[BUFSIZE];
  size_t read = 0;
  auto err = conn.read(buf, sizeof(buf), read);
  if (err != p7r::ERR_SUCCESS) {
    return err;
  }

  // Output
  std::string msg{buf, buf + read};
  LIBLOG_DEBUG("Received from " << conn.peer_addr() << ": " << msg);

  // Echo right back.
  size_t written = 0;
  err = conn.write(buf, read, written);
  if (err == p7r::ERR_ASYNC) {
    return p7r::ERR_SUCCESS;
  }
  return err;
}



p7r::error_t
echo_callback_dgram(p7r::connector conn)
{
  // Read
  char buf[BUFSIZE];
  size_t read = 0;
  liberate::net::socket_address sender;
  auto err = conn.receive(buf, sizeof(buf), read, sender);
  if (err != p7r::ERR_SUCCESS) {
    return err;
  }

  // Output
  std::string msg{buf, buf + read};
  LIBLOG_DEBUG("Received from " << sender << ": " << msg);

  // Echo right back.
  size_t written = 0;
  err = conn.send(buf, read, written, sender);
  if (err == p7r::ERR_ASYNC) {
    return p7r::ERR_SUCCESS;
  }
  return err;
}


p7r::error_t
empty_callback(p7r::connector)
{
  return p7r::ERR_SUCCESS;
}


p7r::error_t
empty_flowid_callback(p7r::util::flowid const &, p7r::connector)
{
  return p7r::ERR_SUCCESS;
}


} // anonymous namespace



TEST(PacketeerUtilBasicClientServer, server_creation)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // We'll register empty callbacks, but we will call all registration
  // functions, if only to exercise the code.
  auto srv = pu::basic_server{
      api,
      scheduler,
      empty_callback
    }
    .on_close(empty_flowid_callback)
    .on_remote(empty_flowid_callback)
    .listen("udp://localhost:54321")
  ;

  // XXX We can't know whether localhost resolves to 1 or 2 addresses
  //     (IPv4/IPv6), so we have to be a bit vague here.
  auto listening = srv.num_listening();
  ASSERT_TRUE(1 <= listening && listening <= 2);
}


TEST(PacketeerUtilBasicClientServer, client_creation)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // We'll register empty callbacks, but we will call all registration
  // functions, if only to exercise the code.
  auto client = pu::basic_client{
      api,
      scheduler,
      empty_callback
    }
    .on_close(empty_flowid_callback)
    .on_remote(empty_flowid_callback)
  ;

  // We're not connecting anywhere without a server running, so assert that the
  // client is not connected.
  ASSERT_FALSE(client.is_connected());
}



TEST(PacketeerUtilBasicClientServer, udp_echo)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // Create a server with an echo callback
  auto srv = pu::basic_server{
      api,
      scheduler,
      echo_callback_dgram
    }
    .listen("udp4://localhost:54321")
  ;

  std::string test_message = "Hello, echo!";

  // Create a client. We need a local address as well for UDP.
  std::string result;

  auto client = pu::basic_client{
      api,
      scheduler,
      [&result](p7r::connector conn) -> p7r::error_t
      {
        // Let's read, using a temp buffer of appropriate size.
        std::vector<char> buf;
        buf.resize(512);

        size_t have_read = 0;
        p7r::peer_address sender;
        auto err = conn.receive(&buf[0], buf.size(), have_read, sender);
        EXPECT_EQ(p7r::ERR_SUCCESS, err);
        EXPECT_NE(0, have_read);

        buf.resize(have_read);

        // Copy the buffer over to the result.
        result = std::string{buf.begin(), buf.end()};

        return p7r::ERR_SUCCESS;
      }
    }
    .connect("udp://127.0.0.1:54321", "udp://127.0.0.1:54322")
  ;

  auto [remote, conn] = *client.connected().begin();

  // Technically, this should be enough to send messages from client to server
  // and vice versa. Since the server implements an echo callback, we write on
  // the client connector.
  size_t written = 0;
  auto err = conn.send(test_message.c_str(), test_message.size(),
      written,
      remote);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(test_message.size(), written);

  // Now we have to let the scheduler do its work. We trigger it twice; once for
  // sending, once for receiving.
  using namespace std::chrono_literals;
  scheduler.process_events(20ms);
  scheduler.process_events(20ms);

  // The result should now be identical to the message
  ASSERT_EQ(test_message, result);
}



TEST(PacketeerUtilBasicClientServer, tcp_echo)
{
  auto api = p7r::api::create();
  auto scheduler = p7r::scheduler{api, p7r::scheduler::SCHED_NO_THREADS};

  // Create a server with an echo callback
  auto srv = pu::basic_server{
      api,
      scheduler,
      echo_callback_stream
    }
    .listen("tcp4://localhost:54321")
  ;

  std::string test_message = "Hello, echo!";

  // Create a client. We need a local address as well for UDP.
  std::string result;

  auto client = pu::basic_client{
      api,
      scheduler,
      [&result](p7r::connector conn) -> p7r::error_t
      {
        // Let's read, using a temp buffer of appropriate size.
        std::vector<char> buf;
        buf.resize(512);

        size_t have_read = 0;
        auto err = conn.read(&buf[0], buf.size(), have_read);
        EXPECT_EQ(p7r::ERR_SUCCESS, err);
        EXPECT_NE(0, have_read);

        buf.resize(have_read);

        // Copy the buffer over to the result.
        result = std::string{buf.begin(), buf.end()};

        return p7r::ERR_SUCCESS;
      }
    }
    .connect("tcp://127.0.0.1:54321")
  ;

  auto conn = client.connected().begin()->second;

  // Technically, this should be enough to send messages from client to server
  // and vice versa. Since the server implements an echo callback, we write on
  // the client connector.
  size_t written = 0;
  auto err = conn.write(test_message.c_str(), test_message.size(),
      written);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(test_message.size(), written);

  // Now we have to let the scheduler do its work. We trigger it twice; once for
  // sending, once for receiving.
  using namespace std::chrono_literals;
  scheduler.process_events(20ms);
  scheduler.process_events(20ms);

  // The result should now be identical to the message
  ASSERT_EQ(test_message, result);
}
