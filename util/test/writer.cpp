/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/writer.h"

#include <liberate/fs/tmp.h>
#include <liberate/fs/path.h>

#include <gtest/gtest.h>

namespace p7r = packeteer;
namespace pu = packeteer::util;

namespace {

struct context
{
  std::shared_ptr<p7r::api>   api;
  p7r::scheduler              scheduler;
  bool                        invoked;
  pu::writer                  writer;

  p7r::connector              client;
  p7r::connector              server_conn;

  inline context()
    : api{p7r::api::create()}
    , scheduler{api, p7r::scheduler::SCHED_NO_THREADS}
    , invoked{false}
    , writer{scheduler, [&](p7r::connector) -> p7r::error_t
      {
        invoked = true;
        return p7r::ERR_SUCCESS;
      }
    }
  {
  }

  inline void init()
  {
    // We create a local connector because it's a socket connector, but does
    // not require networking (technically).
    namespace fs = liberate::fs;
    std::string name = fs::to_posix_path(fs::temp_name("writer-test"));
    auto url = "local://" + name + "?blocking=no";

    p7r::connector server{api, url};
    auto err = server.listen();
    ASSERT_EQ(p7r::ERR_SUCCESS, err);
    ASSERT_TRUE(server.listening());

    client = p7r::connector{api, url};
    err = client.connect();

    server_conn = server.accept();

    ASSERT_TRUE(client.connected());
    ASSERT_TRUE(client.communicating());

    ASSERT_FALSE(client.is_blocking());
    ASSERT_FALSE(server_conn.is_blocking());
  }

};


} // anonymous namespace


TEST(PacketeerUtilInternalWriter, write_without_callback)
{
  context ctx;
  ctx.init();

  // Write to the client connector via the writer must succeed.
  std::string msg{"Hello, world!"};
  size_t written = 0;
  auto err = ctx.writer.write(ctx.client, msg.c_str(), msg.size(), written);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_EQ(msg.size(), written);

  // This is the main thing we're testing here.
  using namespace std::literals::chrono_literals;
  ctx.scheduler.process_events(50ms);
  ASSERT_FALSE(ctx.invoked);
}


TEST(PacketeerUtilInternalWriter, write_with_callback)
{
  context ctx;
  ctx.init();

  // In order to fill the buffer, we have to write successively until the
  // buffer is full. Sounds like a truism, but even writing a GiB in a single
  // call may not have the same effect.
  std::vector<char> buf;
  buf.resize(64 * 1024);

  size_t written = 0;
  p7r::error_t err = p7r::ERR_SUCCESS;
  int count = 0;
  while (p7r::ERR_SUCCESS == err) {
    err = ctx.writer.write(ctx.client, buf.data(), buf.size(), written);
    ++count;
  }
  ASSERT_EQ(p7r::ERR_REPEAT_ACTION, err);
  ASSERT_EQ(0, written);

  // Before draining the buffer, we should not have the callback invoked
  using namespace std::literals::chrono_literals;
  ctx.scheduler.process_events(50ms);
  ASSERT_FALSE(ctx.invoked);

  // Let's read on the server connection to drain the buffer.
  err = p7r::ERR_SUCCESS;
  while (p7r::ERR_SUCCESS == err) {
    size_t read = 0;
    err = ctx.server_conn.read(&buf[0], buf.size(), read);
  }

  // This is the main thing we're testing here. We'll need for the scheduler
  // to do it's thing first, though.
  ctx.scheduler.process_events(50ms);
  ASSERT_TRUE(ctx.invoked);
}
