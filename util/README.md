Utility Functionality
=====================

Packeteer provides functionality for reading and writing from socket-like
constructs, but generally tries to stay very much out of the way when it comes
to managing them.

This leaves some complexity up to the user that in many cases can be taken off
their hands. The functionality in this sub-library offers ways to do just that.

In particular, this library addresses the following problems:

- Handling multiple connections in servers. Packeteer's basic functionality
  requires you two write an accept() loop yourself.
- Abstracting out much of the difference between datagram and stream
  connectors. Datagram connectors do not support accept() in the same way that
  stream connectors do. Additionally, the concept of an established connection
  does not really exist - but is a useful abstraction when writing code.
- Dealing with reading from connectors in a timely fashion to free kernel
  I/O buffers.
- Dealing with connectors that are currently not writeable, for example due to
  congestion.

Basic Client and Server
-----------------------

Basic server addresses handling accept() calls for you, and basic client
provides a client side counter-part. Whether provided with a stream or datagram
connector, the basic server accepts a read callback in its constructor that
is invoked when a connector becomes readable.

In stream connectors, the main connector to listen on becomes readable when
a peer connected; it is then desirable to accept() the connection. The accepted
connector may in turn become readable at a later stage. The basic server
performs this accept() and registers a read callback for you. As a consequence,
the read callback provided to the server will only be invoked when there is a
peer that has connected and has written something.

```c++

auto srv = basic_server{api, scheduler,
  [](connector conn) -> error_t
  {
    // It's safe to read from this connector. Just be careful to use receive()
    // vs. read() depending on the type of connector.
  });
```

Similarly, `on_remote` and `on_close` provide facilities via which the life
cycle of these connectors can be managed.


Flow Manager
------------

The detail `flow_manager` class provides functionality for handling the life
cycle of connectors as passed to callbacks to the basic client or server.

To this end, it introduces the concept of a `flow`. The term is borrowed from
TCP, and can be viewed as conceptually the same from a user point of view, in
that it represents a data flow between two peers. Unlike TCP, there is no
guarantee of managing network traffic across various hops between source and
destination, i.e. a packeteer `flow` for UDP may send each packet along a
different route, while a TCP flow (and a packeteer `flow` for TCP) would not.

The flow manager class is something of an inofficial part of the API; you can
use it, but it's best to consider it experimental. It is, however, part of the
client and server classes (below).

I/O Queues
----------

I/O queues are a way for decoupling writes from the application from writes to
a connector and vice versa for reads. For the purposes of not blocking the
network, it is often desirable to read from sockets immediately and process
the data when the CPU is free to do so. In such a case, a read loop should
place data into a queue.

Writing may benefit from similar handling. A socket may not be writable due
to network congestion, but the application has data to send right now. Placing
such data into a queue to be written when that is possible may help with
writing performant I/O code.

Please be aware that there are pitfalls to this approach. Congestions
management for networking protocols is often based on whether kernel I/O
buffers are full. Draining them without being able to process the data may
free the peer to send more, but would shift the bottleneck to the receiver's
CPU. Using I/O queues in this fashion is only sensible if there is sufficient
CPU available to process data at speed.

The library provides an abstract I/O queue interface, as well as a concurrent
in-memory queue as an initial implementation. Other implementations could e.g.
write to storage for persistent store-and-forward operations, etc.

Managed Writes
--------------

I/O queues can help when an application produces more data than can be written
to a connector. The other part of the write equation, though, is when data
could be written, but none is available.

The packeteer scheduler permits registering write callbacks to be invoked when
a connector is writable. Doing so, however, can result in this callback being
invoked over and over for little reason, which at minimum eats CPU time for no
good reason.

An implementation detail of the library, a writer class manages this by trying
to write to a connector, and only upon failure registering a write callback.
When that is invoked, the write is attempted again.

Client and Server
-----------------

The client and server classes combine the concerns outlined above. They build
upon the basic client and server, but combine this with flow manager to provide
an interface that works on established flows.

For each flow, I/O queues are created - by default, the concurrent in-memory
queue is used. Finally, writes are scheduled via the writer, such that the
application can always write (to the queue), while the library drains the queue
to the socket when that is possible.

This interface then also factors out the differences between datagram and stream
connectors entirely, with the exception of the connect() functions. That means
that `flow` (see above) no longer offers receive()/send() functions, but
contains an id that identifiers sender and recipient peer addresses for any kind
of connector.

A complete echo server becomes very simple, then, and the client similarly so
(see unit tests for full example):

```c++
error_t
echo_callback(flow the_flow)
{
  // Read
  char buf[BUFSIZE];
  size_t read = 0;
  auto err = the_flow.read(buf, sizeof(buf), read);
  if (err != ERR_SUCCESS) {
    return err;
  }

  // Output
  std::string msg{buf, buf + read};
  LIBLOG_DEBUG("Received from " << the_flow.id() << ": " << msg);

  // Echo right back.
  size_t written = 0;
  err = the_flow.write(buf, read, written);
  if (err == ERR_ASYNC) {
    return ERR_SUCCESS;
  }
  return err;
}


auto srv = server{api, scheduler, TIMEOUT,
  echo_callback
}.listen(CONNECT_URL);

// Event loop stuff...
scheduler.process_events(...);
```

As can be seen, the flow's read() and write() functions are all that's
necessary to know in the server's read callback. The flow's id will
contain both the local and the peer address of the flow.
