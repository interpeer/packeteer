/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_SERVER_H
#define PACKETEER_UTIL_SERVER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <functional>

#include <set>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <liberate/net/url.h>

#include <packeteer/util/callbacks.h>
#include <packeteer/util/flow.h>
#include <packeteer/util/io_queue.h>
#include <packeteer/util/io_queue/concurrent_memory_queue.h>

namespace packeteer::util {

/**
 * Similar to the client, the server class combines a basic_server with a
 * flow_manager and I/O queue and a writer that schedules I/O when the
 * socket can accept it.
 *
 * As a result, the server is probably what most programmers would expect,
 * except it factors out most differences between stream and datagram
 * connectors.
 *
 * Servers can listen{_on}() multiple endpoints. Every established flow
 * triggers an on_remote() callback. Each is passed an established flow
 * instance that then provides read() and write() functions as required.
 */
class PACKETEER_API server
{
public:
  /**
   * Constructor; needs to take an API instance and scheduler.
   *
   * The timeout is used to determine when a flow should be considered dead.
   * The read_cb is invoked when the endpoint of a flow has sent data. Lastly,
   * the queue_creator is used whenever a new flow is established. You can pass
   * any function returning an io_queue here.
   */
  server(std::shared_ptr<api> api, scheduler & sched, duration timeout,
      timepoint_flow_callback read_cb,
      io_queue_creator queue_creator = create_concurrent_memory_queue
  );

  inline server(std::shared_ptr<api> api, scheduler & sched, duration timeout,
      simple_flow_callback read_cb,
      io_queue_creator queue_creator = create_concurrent_memory_queue
  )
    : server{api, sched, timeout,
        erase_timepoint(read_cb),
        queue_creator
      }
  {
  }


  inline ~server() = default;


  /**
   * The server can optionally take a callback when a new flow is created, or a
   * flow closes.
   */
  server & on_remote(simple_flow_callback remote_cb);
  server & on_remote(timepoint_flow_callback remote_cb);

  server & on_close(simple_flow_callback close_cb);
  server & on_close(timepoint_flow_callback close_cb);

  /**
   * The server can listen on as many URLs as it wants.
   *
   * We typically prefer to avoid exceptions, but they do make the builder
   * pattern easier to implement. We therefore offer a builder-friendly
   * listen() version of this function, and a exception-avoiding listen_on().
   *
   * The versions of these functions accepting strings or URLs perform
   * DNS lookups (if necessary) and will listen to *all* resulting addresses.
   */
  error_t listen_on(liberate::net::url const & conn_url);
  server & listen(liberate::net::url const & conn_url);

  template <typename urlT>
  error_t
  listen_on(urlT const & conn_url)
  {
    return listen_on(liberate::net::url::parse(conn_url));
  }

  template <typename urlT>
  server &
  listen(urlT const & conn_url)
  {
    return listen(liberate::net::url::parse(conn_url));
  }


  /**
   * Get information about the connectors we're listening on. These are not
   * full flows yet, but simple connectors.
   */
  std::set<connector> const & listening() const;
  std::size_t num_listening() const;


  /**
   * Map of *established* flows.
   */
  flow_map const & established() const;
  std::size_t num_established() const;


  /**
   * Close a flow. If the last flow via a given connector is closed, the
   * connector is also closed. Conversely, closing the connector closes
   * all associated flows.
   */
  error_t close(flowid const & id);
  error_t close(flow const & the_flow);

private:
  struct server_impl;
  std::shared_ptr<server_impl>  m_impl;
};

} // namespace packeteer::util

#endif // guard
