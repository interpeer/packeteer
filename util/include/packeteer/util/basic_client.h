/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_BASIC_CLIENT_H
#define PACKETEER_UTIL_BASIC_CLIENT_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <liberate/net/url.h>

#include <set>

#include <packeteer/util/callbacks.h>


namespace packeteer::util {

/**
 * The purpose of this basic_client class is to factor out differences between
 * stream and datagram oriented connectors. In practice, there is very little
 * diference, but the interface of this class makes it more in line with its
 * sibling server class.
 *
 * As with the server class, it allows registering callbacks when connectors
 * close or a connection is established successfully, but similar restrictions
 * apply here with regards to datagram oriented connectors.
 */
class PACKETEER_API basic_client
{
public:
  /**
   * Constructors need to take callbacks.
   */
  basic_client(std::shared_ptr<api> api, scheduler & sched,
      simple_callback read_cb);
  basic_client(std::shared_ptr<api> api, scheduler & sched,
      timepoint_callback read_cb);

  /**
   * The basic_client can optionally take a callback when a peer was detected, or
   * when a close event was issued.
   */
  basic_client & on_remote(simple_flowid_callback remote_cb);
  basic_client & on_remote(timepoint_flowid_callback remote_cb);

  basic_client & on_close(simple_flowid_callback close_cb);
  basic_client & on_close(timepoint_flowid_callback close_cb);

  /**
   * Use basic_client to establish a connection to a remote server endpoint.
   *
   * The basic_client allows you to establish multiple connections, either to
   * the same or to remote endpoints. This is not typically how most clients
   * would operate, but there are cases where this is desirable. Note, though,
   * that this means each of the connected() map is essentially independent of
   * the others.
   *
   * We typically prefer to avoid exceptions, but they do make the builder
   * pattern easier to implement. We therefore offer a builder-friendly
   * connect() version of this function, and a exception-avoiding connect_to().
   *
   * These functions perform DNS lookup for connection URLs. They filter the
   * results to matching tuples, e.g. a local IPv4 to a remote IPv4 address,
   * and perform a connection attempt for each matching tuple.
   *
   * Of course, if no local URL is provided, then a "match" is easily found,
   * but the ordering of results is still not determinate.
   */
  error_t connect_to(liberate::net::url const & remote_url, liberate::net::url const & local_url = {});
  basic_client & connect(liberate::net::url const & remote_url, liberate::net::url const & local_url = {});

  template <typename urlT1, typename urlT2 = urlT1>
  error_t connect_to(urlT1 const & remote_url, urlT2 const & local_url = {})
  {
    liberate::net::url local;
    try {
      local = liberate::net::url::parse(local_url);
    } catch (...) {}

    return connect_to(
        liberate::net::url::parse(remote_url),
        local
    );
  }

  template <typename urlT1, typename urlT2 = urlT1>
  basic_client & connect(urlT1 const & remote_url, urlT2 const & local_url = {})
  {
    liberate::net::url local;
    try {
      local = liberate::net::url::parse(local_url);
    } catch (...) {}

    return connect(
        liberate::net::url::parse(remote_url),
        local
    );
  }

  /**
   * Are we connected, yes or no?
   */
  bool is_connected() const;

  /**
   * Access the internal connectors, and the remote URLs they're connected
   * to.
   */
  using connector_map = std::map<peer_address, connector>;
  connector_map const & connected() const;

  /**
   * Closing a connector via this function also removes it from
   * listening and established sets, as appropriate.
   */
  error_t close(connector conn);


private:
  struct client_impl;
  std::shared_ptr<client_impl>  m_impl;
};

} // namespace packeteer::util

#endif // guard
