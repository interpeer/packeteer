/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_CALLBACKS_H
#define PACKETEER_UTIL_CALLBACKS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/error.h>
#include <packeteer/connector.h>

#include <packeteer/util/flowid.h>
#include <packeteer/util/flow.h>

#include <functional>

namespace packeteer::util {

/**
 * In the util library, we provide simpler callbacks: the connector is by
 * reference, and we provide a choice for callbacks with or without time_point
 * parameter. Events are not passed, as re-use of a single callback for
 * multiple evnets is discouraged.
 */
using simple_callback = std::function<
  packeteer::error_t (packeteer::connector)
>;
using timepoint_callback = std::function<
  packeteer::error_t (packeteer::time_point const &, packeteer::connector)
>;


/**
 * Some callback exists to manage associations between flow identifiers (flowid)
 * and connectors. In some cases, only the local or remote part of the flowid is
 * known, and we have to distinguish which it is.
 */
using simple_flowid_callback = std::function<
  packeteer::error_t (flowid const &, packeteer::connector)
>;
using timepoint_flowid_callback = std::function<
  packeteer::error_t (packeteer::time_point const &, flowid const &, packeteer::connector)
>;


/**
 * Finally, some callbacks just provide you a flow instance immediately.
 */
using simple_flow_callback = std::function<
  packeteer::error_t (flow)
>;
using timepoint_flow_callback = std::function<
  packeteer::error_t (packeteer::time_point const &, flow)
>;



/**
 * A simple helper function creates a timepoint_callback from a callback (a
 * lambda that ignores the time_point parameter.
 */
inline timepoint_callback
erase_timepoint(simple_callback cb)
{
  return [cb](time_point const &, connector conn) -> error_t
  {
    return cb(conn);
  };
}

inline timepoint_flowid_callback
erase_timepoint(simple_flowid_callback cb)
{
  return [cb](time_point const &, flowid const & id, connector conn) -> error_t
  {
    return cb(id, conn);
  };
}

inline timepoint_flow_callback
erase_timepoint(simple_flow_callback cb)
{
  return [cb](time_point const &, flow f) -> error_t
  {
    return cb(f);
  };
}



} // namespace packeteer::util

#endif // guard
