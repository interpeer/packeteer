/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_FLOWID_H
#define PACKETEER_UTIL_FLOWID_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <ostream>
#include <sstream>

#include <liberate/cpp/operators.h>
#include <liberate/cpp/hash.h>

#include <packeteer/connector/peer_address.h>

namespace packeteer::util {

/**
 * See flow.h for context.
 *
 * The flowid is an association of a local and remote peer by peer address.
 * Any data that flows from the local to the remote address or vice versa
 * can be considered part of a conceptual data flow.
 *
 * Flow identifiers have complements that swap the local and remote parts.
 * It is impossible for a local peer to communicate on a remote address, of
 * course, but the remote peer identifies the same flow by a complementary
 * flowid. It's worth being able to represent this.
 */
struct PACKETEER_API flowid
  : public ::liberate::cpp::comparison_operators<flowid>
{
  peer_address local = {};
  peer_address remote = {};

  inline flowid()
  {}

  inline flowid(peer_address const & _local, peer_address const & _remote = peer_address{})
    : local{_local}
    , remote{_remote}
  {
  }

  inline flowid(flowid const &) = default;
  inline flowid(flowid &&) = default;
  inline flowid & operator=(flowid const &) = default;

  // Make this class hashable
  inline std::size_t hash() const
  {
    return liberate::cpp::multi_hash(local, remote);
  }

  // Is a local or remote flowid *only*? Is it a full flowid?
  inline bool is_local() const
  {
    return local != peer_address{} && remote == peer_address{};
  }

  inline bool is_remote() const
  {
    return local == peer_address{} && remote != peer_address{};
  }

  inline bool is_full() const
  {
    return local != peer_address{} && remote != peer_address{};
  }


  // In addition to comparison, we want to know if two identifiers are
  // complements and/or create the complement.
  inline bool
  is_complement(flowid const & other) const
  {
    return (remote == other.local && local == other.remote);
  }

  inline flowid
  make_complement() const
  {
    return {remote, local};
  }

  inline bool is_equal_to(flowid const & other) const
  {
    return (local == other.local && remote == other.remote);
  }

  inline bool is_less_than(flowid const & other) const
  {
    if (local < other.local) {
      return true;
    }
    if (remote < other.remote) {
      return true;
    }
    return false;
  }

  inline std::string str() const
  {
    std::stringstream sstream;
    if (is_local()) {
      sstream << "[" << local << " (local)]";
    }
    else if (is_remote()) {
      sstream << "[" << remote << " (remote)]";
    }
    else {
      sstream << "[" << local << "<->" << remote << "]";
    }

    return sstream.str();
  }
};



inline std::ostream &
operator<<(std::ostream & os, flowid const & id)
{
  os << id.str();
  return os;
}


} // namespace packeteer::util


LIBERATE_MAKE_HASHABLE(packeteer::util::flowid)

#endif // guard
