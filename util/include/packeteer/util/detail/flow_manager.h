/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_DETAIL_FLOW_MANAGER_H
#define PACKETEER_UTIL_DETAIL_FLOW_MANAGER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#ifndef PACKETEER_UTIL_USE_DETAIL
#error You are trying to include a detail header. This header is not \
  providing a stable interface. If you know what you are doing, define \
  PACKETEER_UTIL_USE_DETAIL to override this error.
#endif

#include <packeteer.h>

#include <functional>
#include <map>
#include <set>
#include <mutex>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <packeteer/util/flowid.h>
#include <packeteer/util/callbacks.h>

#include <liberate/logging.h>

namespace packeteer::util::detail {

/**
 * The flow_manager class provides on_close and on_remote callbacks suitable
 * for use with basic_client and basic_server. It wraps other callbacks
 * provided by the user, and takes care of managing flow instances.
 *
 * The fundamental operations are, of course, to add and remove flowids to
 * a collection (and do nothing if that action was not required).
 *
 * On top of that, the class provides easy callbacks for integration with
 * basic_server and basic_client.
 *
 * We pimpl flow manager for easier copying.
 */
struct flow_manager
{
  /**
   * Constructor. At minimum, we need a scheduler.
   *
   * A timeout means that any flow added at a particular time (by specifying
   * the time parameter) will be closed and removed when the timeout is
   * reached.
   */
  inline flow_manager(scheduler & _scheduler, duration const & timeout = {})
    : m_impl{std::make_shared<flow_manager_impl>(_scheduler, timeout)}
  {
  }


  inline ~flow_manager() = default;

  /**
   * Add on_remote/on_close callbacks
   */
  inline flow_manager &
  on_remote(timepoint_flowid_callback callback)
  {
    m_impl->on_remote(callback);
    return *this;
  }

  inline flow_manager &
  on_remote(simple_flowid_callback callback)
  {
    m_impl->on_remote(erase_timepoint(callback));
    return *this;
  }

  inline flow_manager &
  on_close(timepoint_flowid_callback callback)
  {
    m_impl->on_close(callback);
    return *this;
  }

  inline flow_manager &
  on_close(simple_flowid_callback callback)
  {
    m_impl->on_close(erase_timepoint(callback));
    return *this;
  }


  /**
   * Basic mapping functionality
   */
  inline error_t
  add(flowid const & id, connector conn, time_point const & time = {})
  {
    return m_impl->add(id, conn, time);
  }



  inline error_t
  remove(flowid const & id, connector conn = {}, time_point const & time = {})
  {
    return m_impl->remove(id, conn, time);
  }



  inline bool
  contains(flowid const & id)
  {
    return m_impl->contains(id);
  }



  /**
   * Callback style function for use with std::bind for adding new flows
   */
  inline error_t
  remote_callback(time_point const & time, flowid const & id, connector conn)
  {
    return m_impl->add(id, conn, time);
  }

  inline error_t
  close_callback(time_point const & time, flowid const & id, connector conn)
  {
    return m_impl->remove(id, conn, time);
  }


private:


  struct flow_manager_impl
  {
    inline flow_manager_impl(scheduler & _sched, duration timeout)
      : m_scheduler{_sched}
      , m_timeout{timeout}
    {
    }

    scheduler &                 m_scheduler;
    duration                    m_timeout;

    timepoint_flowid_callback   m_remote;
    timepoint_flowid_callback   m_close;

    using connector_entry = std::tuple<connector, time_point>;
    using connector_map = std::map<peer_address, connector_entry>;
    using flow_map = std::map<peer_address, connector_map>;

    flow_map                    m_managed;

    // Need to manage concurrent access
    std::mutex                  m_mutex;


    inline ~flow_manager_impl()
    {
      std::unique_lock<std::mutex> lock{m_mutex};
      m_remote = m_close = nullptr;
    }


    /**
     * Add on_remote/on_close callbacks
     */
    inline void
    on_remote(timepoint_flowid_callback callback)
    {
      std::unique_lock<std::mutex> lock{m_mutex};
      m_remote = callback;
    }

    inline void
    on_close(timepoint_flowid_callback callback)
    {
      std::unique_lock<std::mutex> lock{m_mutex};
      m_close = callback;
    }


    /**
     * Basic mapping functionality
     */
    inline error_t
    add(flowid const & id, connector conn, time_point const & time = {})
    {
      if (id.is_remote()) {
        LIBLOG_ERROR("Cannot add remote-only flowids");
        return ERR_INVALID_VALUE;
      }
      LIBLOG_DEBUG("Adding flow: " << id);

      auto now = time;
      if (now == time_point{}) {
        // XXX Could couple this more loosely with a lambda
        now = clock::now();
      }

      std::unique_lock<std::mutex> lock{m_mutex};

      auto iter = m_managed.find(id.local);
      if (iter == m_managed.end()) {
        auto [it, _] = m_managed.insert({id.local, {}});
        iter = it;
      }

      auto iter2 = iter->second.find(id.remote);
      if (iter2 == iter->second.end()) {
        iter->second[id.remote] = std::make_tuple(conn, now);

        // Also register our own internal remove function.
        m_scheduler.register_connector(PEV_IO_CLOSE, conn,
            [this, id](time_point const & tp, events_t, connector * c) -> error_t
            {
              return remove(id, *c, tp);
            },
            IO_FLAGS_ONESHOT
        );

        // Update the timeout callback
        update_timeouts(now);

        // Call m_remote; we should only do this here when a *new* flow is
        // found.
        if (m_remote) {
          return m_remote(now, id, conn);
        }
        return ERR_SUCCESS;
      }

      if (std::get<0>(iter2->second) == conn) {
        return ERR_SUCCESS;
      }

      LIBLOG_ERROR("Cannot add flow, it already exists.");
      return ERR_INVALID_VALUE;
    }



    inline error_t
    remove(flowid const & id, connector conn = {}, time_point const & time = {})
    {
      std::unique_lock<std::mutex> lock{m_mutex};

      LIBLOG_DEBUG("Removing flow: " << id);

      if (id.is_full()) {
        auto [ret, _] = remove_full(id, conn, time, lock);
        return ret;
      }

      auto now = time;
      if (now == time_point{}) {
        // XXX Could couple this more loosely with a lambda
        now = clock::now();
      }

      error_t ret = ERR_SUCCESS;
      std::set<packeteer::connector> to_close;

      if (id.is_remote()) {
        // Remove from all local maps where the remote address matches the
        // connector's peer_addr()
        auto local_iter = m_managed.begin();
        for ( ; local_iter != m_managed.end() ; ) {
          auto remote_iter = local_iter->second.find(id.remote);
          if (remote_iter == local_iter->second.end()) {
            ++local_iter;
            continue;
          }

          to_close.insert(std::get<0>(remote_iter->second));

          local_iter->second.erase(remote_iter);
          if (local_iter->second.empty()) {
            local_iter = m_managed.erase(local_iter);
          }
          else {
            ++local_iter;
          }
        }
      }
      else {
        // If there is a local address, we need to remove the entire inner map
        auto local_iter = m_managed.find(id.local);
        if (local_iter != m_managed.end()) {
          auto remote_iter = local_iter->second.begin();
          for ( ; remote_iter != local_iter->second.end() ; ++remote_iter) {
            to_close.insert(std::get<0>(remote_iter->second));
          }

          m_managed.erase(local_iter);
        }
      }

      if (to_close.empty()) {
        return ERR_INVALID_VALUE;
      }

      // Update remaining timeouts
      update_timeouts(now);

      // Unlock now to avoid recursive locking issues.
      lock.unlock();

      if (conn.type() != CT_UNSPEC) {
        to_close.insert(conn);
      }
      for (auto & close_conn : to_close) {
        auto r = _call_close(now, id, close_conn);
        if (ERR_SUCCESS != r) {
          ret = r;
        }
      }

      return ret;
    }



    inline bool
    contains(flowid const & id)
    {
      std::unique_lock<std::mutex> lock{m_mutex};

      LIBLOG_DEBUG("Checking for flow: " << id);

      // Check local only for a local id
      auto local_iter = m_managed.find(id.local);
      if (local_iter == m_managed.end()) {
        if (!id.is_remote()) {
          return false;
        }
      }
      else {
        if (id.is_local()) {
          return true;
        }
      }

      // Check remote and local for a full id.
      if (id.is_full()) {
        auto remote_iter = local_iter->second.find(id.remote);
        return remote_iter != local_iter->second.end();
      }

      // Check remote only (but for all locals) for a remote id
      for (auto & [_, map] : m_managed) {
        auto iter = map.find(id.remote);
        if (iter != map.end()) {
          return true;
        }
      }

      return false;
    }



    inline std::tuple<error_t, connector>
    remove_full(flowid const & id, connector conn [[maybe_unused]], time_point const & time,
        std::unique_lock<std::mutex> & lock)
    {
      auto now = time;
      if (now == time_point{}) {
        // XXX Could couple this more loosely with a lambda
        now = clock::now();
      }

      // Removing an entire flow id means we have to match the local *and*
      // remote parts.
      auto local_iter = m_managed.find(id.local);
      if (local_iter == m_managed.end()) {
        LIBLOG_ERROR("Cannot find local part");
        return {ERR_INVALID_VALUE, {}};
      }

      // Now for the local part.
      auto remote_iter = local_iter->second.find(id.remote);
      if (remote_iter == local_iter->second.end()) {
        LIBLOG_ERROR("Cannot find remote part");
        return {ERR_INVALID_VALUE, {}};
      }

      auto found_conn = std::get<0>(remote_iter->second);

      local_iter->second.erase(remote_iter);
      if (local_iter->second.empty()) {
        m_managed.erase(local_iter);
      }

      update_timeouts(now);

      // Unlock now to avoid recursive locking issues.
      lock.unlock();

      // Only when bookkeping is done, call the close callback and exit.
      auto ret = _call_close(now, id, found_conn);
      return {ret, found_conn};
    }


    inline error_t
    _call_close(time_point const & time, flowid const & id, connector conn)
    {
      // No longer interested in events for a to-be-closed connector
      m_scheduler.unregister_connector(conn);
      try {
        m_scheduler.commit_callbacks();
      } catch (packeteer::exception const & ex) {
        LIBLOG_EXC(ex, "Error committing unregistration, ignoring.");
      } catch (std::exception const & ex) {
        LIBLOG_EXC(ex, "Error committing unregistration, ignoring.");
      } catch (...) {
        LIBLOG_ERROR("Unknown error committing unregistration, ignoring.");
      }

      // Close connector
      if (conn.communicating()) {
        conn.close();
      }

      if (!m_close) {
        return ERR_SUCCESS;
      }

      return m_close(time, id, conn);
    }

    inline void
    update_timeouts(time_point const & now [[maybe_unused]])
    {
      // Skip if no timeout is configured
      if (m_timeout == duration{}) {
        return;
      }

      // We have to find the nearest point in the future at which a connector
      // expires.
      time_point next = time_point::max();
      flowid id;

      for (auto & [local, map] : m_managed) {
        for (auto & [remote, entry] : map) {
          time_point candidate = std::get<1>(entry) + m_timeout;
          if (candidate < next) {
            id.local = local;
            id.remote = remote;
            next = candidate;
          }
        }
      }

      // Schedule callback. We might have other callbacks from previous calls
      // to this function also expire; we can ignore this here.
      callback cb = [this, id](time_point const & time, events_t, connector *) -> error_t
      {
        std::unique_lock<std::mutex> lock{m_mutex};

        // Remove the connector
        auto [ret, conn] = remove_full(id, {}, time, lock);

        // Update callbacks; schedules the next one
        update_timeouts(time);

        lock.unlock();

        return _call_close(time, id, conn);
      };
      m_scheduler.schedule_at(next, cb);
      // If there are exceptions here, we want to know.
      m_scheduler.commit_callbacks();
    }
  };

  std::shared_ptr<flow_manager_impl> m_impl;
};



} // namespace packeteer::util::detail

#endif // guard
