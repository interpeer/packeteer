/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_BASIC_SERVER_H
#define PACKETEER_UTIL_BASIC_SERVER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <set>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <liberate/net/url.h>

#include <packeteer/util/callbacks.h>

namespace packeteer::util {

/**
 * The purpose of this basic_server class is to factor out differences between
 * stream and datagram oriented connectors. In practice, this means handling
 * accept() calls at the appropriate time for the stream variants.
 *
 * The class also allows registering a callback when connectors close, or when
 * a new remote is connecting. The problem is that without actively reading,
 * this only works for stream oriented connectors, as datagram connectors don't
 * establish connections as such. Therefore, the behaviour of stream and
 * datagram connectors is somewhat different:
 *
 * STREAM:
 *  - on_remote will be called when a peer connects
 *  - on_close will be called for all established peer connectors as well as
 *    the listening connectors
 * DATAGRAM:
 *  - on_remote will not be called
 *  - on_close will be called for listening connectors
 *
 * If you need connection-like management for datagram connectors, unfortunately
 * this can only be achieved with the help of some additional classes in this
 * library.
 */
class PACKETEER_API basic_server
{
public:
  /**
   * Constructors need to take callbacks.
   */
  basic_server(std::shared_ptr<api> api, scheduler & sched,
      simple_callback read_cb);
  basic_server(std::shared_ptr<api> api, scheduler & sched,
      timepoint_callback read_cb);

  /**
   * The basic_server can optionally take a callback when a peer was detected, or
   * when a close event was issued.
   */
  basic_server & on_remote(simple_flowid_callback remote_cb);
  basic_server & on_remote(timepoint_flowid_callback remote_cb);

  basic_server & on_close(simple_flowid_callback close_cb);
  basic_server & on_close(timepoint_flowid_callback close_cb);

  /**
   * The basic_server can listen on as many connectors as it wants; the only
   * requirement is that the connector is not communicating(); it may
   * be listening().
   *
   * We typically prefer to avoid exceptions, but they do make the builder
   * pattern easier to implement. We therefore offer a builder-friendly
   * listen() version of this function, and a exception-avoiding listen_on().
   *
   * The versions of these functions accepting strings or URLs perform
   * DNS lookups (if necessary) and will listen to *all* resulting addresses.
   */
  error_t listen_on(liberate::net::url const & conn_url);
  basic_server & listen(liberate::net::url const & conn_url);

  template <typename urlT>
  error_t
  listen_on(urlT const & conn_url)
  {
    return listen_on(liberate::net::url::parse(conn_url));
  }

  template <typename urlT>
  basic_server &
  listen(urlT const & conn_url)
  {
    return listen(liberate::net::url::parse(conn_url));
  }

  /**
   * Get information about the connectors we're listening or
   * communicating on.
   */
  std::set<connector> const & listening() const;
  std::size_t num_listening() const;

  std::set<connector> const & established() const;
  std::size_t num_established() const;


  /**
   * Closing a connector via this function also removes it from
   * listening and established sets, as appropriate.
   */
  error_t close(connector conn);

private:
  struct server_impl;
  std::shared_ptr<server_impl>  m_impl;
};

} // namespace packeteer::util

#endif // guard
