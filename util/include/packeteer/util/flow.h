/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_FLOW_H
#define PACKETEER_UTIL_FLOW_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <memory>
#include <map>

#include <liberate/cpp/operators/comparison.h>

#include <packeteer/connector.h>

#include <packeteer/util/flowid.h>

namespace packeteer::util {

/**
 * The "flow" class resembles what would usually be called a "connection", but
 * since it also applies to connection-less connectors, this is the better
 * term: it's an abstraction for communicating from a single local to a single
 * remote address and back.
 *
 * Of course, the "flow" term is borrowed from TCP, and connection-less
 * connectors may not have anything resembling a TCP-like "flow" in their
 * implementation. Still, the term is better than introducing a "connection"
 * term that clashes with the "connector" of the main packeteer library.
 *
 * You do not create flow instances yourself; they are managed entirely by the
 * client and server instances respectively. All you can do is access them.
 *
 * Flows provide information on their local and remote addresses, as well as
 * read() and write() methods. There is no send()/receive() equivalent; these
 * functions will be used internally where appropriate (aka when connection-less
 * connectors are in use).
 *
 * Flows are effectively represented by an identifier consisting of a local and
 * a remote peer address.
 */
class PACKETEER_API flow
  : public ::liberate::cpp::comparison_operators<flow>
{
public:
  /**
   * Copy-constructible, assignable, etc. All copies of a flow refer to the
   * same flow and flow state.
   */
  flow(flow const &) = default;
  flow(flow &&) = default;
  flow & operator=(flow const &) = default;

  /**
   * The identifier of this flow.
   */
  flowid id() const;

  /**
   * The connector the flow is associated with.
   */
  packeteer::connector conn() const;

  /**
   * I/O functions; see packeteer::connector for details.
   */
  error_t read(void * buf, size_t bufsize, size_t & bytes_read);
  error_t write(void const * buf, size_t bufsize, size_t & bytes_written);

  bool communicating() const;
  size_t peek() const;


  /**
   * Close a flow. This may close the underlying connector, if it's the last
   * flow to use it.
   */
  error_t close();


  inline std::size_t hash() const
  {
    return id().hash();
  }

  inline bool is_equal_to(flow const & other) const
  {
    return id() == other.id();
  }

  inline bool is_less_than(flow const & other) const
  {
    return id() < other.id();
  }

private:
  friend class client;
  friend class server;

  struct flow_impl;
  std::shared_ptr<flow_impl>  m_impl;

  // Pass ownership of impl to flow class.
  flow(std::unique_ptr<flow_impl> impl);

  // Create a new flow that shares an impl with an existing one.
  flow(std::shared_ptr<flow_impl> impl);
};


/**
 * Default flow map
 */
using flow_map = std::map<flowid, flow>;

} // namespace packeteer::util

LIBERATE_MAKE_HASHABLE(packeteer::util::flow)

#endif // guard
