/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_IO_QUEUE_H
#define PACKETEER_UTIL_IO_QUEUE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <liberate/types/byte.h>

#include <functional>
#include <memory>

namespace packeteer::util {

/**
 * The purpose of an I/O queue is to permit concurrent threads to buffer data
 * after recieving from or before sending to a connector.
 *
 * There are a few implications of this.
 *  1.) The operations offered here should be fast as not to block multiple
 *      threads.
 *  2.) The operations must permit concurrent access.
 *    a) The operations should be atomic.
 *  3.) Having read data from a connector, there is no putting it back. Pushing
 *      data into an I/O queue shoud therefore typically succeed. There is of
 *      course such a thing as a limited size FIFO, where a failure may occur
 *      and must be handled. But it should be clear that a failure for a queue
 *      to accept new data results in the loss of said data.
 *  4.) Conversely, writing to a connector may fail temporarily. That means
 *      that taking data from the queue should not be final. Either it must
 *      be possible to put it back, or the read access and removal must be
 *      logically decoupled.
 *    a) Note that decoupling, e.g. via front() and pop() as some STL containers
 *       do has a distinct downside, which is that multiple threads calling
 *       front() will receive the same data.
 *    b) Note that putting back data, e,g. via pop_front() and push_front() as
 *       some STL containers do has the distinct downside that if multiple
 *       threads fail, the push_front() function should be invokable multiple
 *       times without losing data.
 *
 * We're choosing 4b) in the above choices.
 */
class PACKETEER_API io_queue
{
public:
  virtual ~io_queue() = default;

  /**
   * A successful use of push() should result in a data callback function to
   * be invoked. The callback prototype and setting function are provied here.
   */
  using data_callback = std::function<void (io_queue &)>;
  inline void set_data_callback(data_callback cb)
  {
    m_data_cb = cb;
  }


  /**
   * Return whether there are items in the queue. Note that in a concurrent
   * environment, there is a potential for a race condition between calling
   * this and popping an entry; it's usually better to try the latter and
   * check for success.
   */
  virtual bool empty() const = 0;

  /**
   * Return the current number of entries in the queue. The same warning
   * applies as with empty() above. Additionally, this operation may be
   * expensive.
   *
   * Implementations may choose to return a negative value to indicate that
   * the function is not supported.
   */
  virtual ssize_t size() const = 0;


  /**
   * The main write function. Takes a buffer and buffer size, and returns if
   * the entry has been accepted.
   */
  virtual bool push_back(liberate::types::byte const * buf, std::size_t bufsize) = 0;

  inline bool push_back(void const * buf, std::size_t bufsize)
  {
    return push_back(reinterpret_cast<liberate::types::byte const *>(buf),
        bufsize);
  }


  /**
   * The main read function. Takes a buffer and buffer size. The return value
   * is the number of bytes read from the queue and written to the buffer.
   *
   * A return value less than the buffer size does not indicate that the queue
   * is empty afterwards. The queue may use contiguous memory or chunked memory
   * internally, and this may just mean that the chunk read was smaller than
   * the provided buffer.
   *
   * Similarly, a value of zero does not mean that the queue is empty. It just
   * indicates that zero bytes were read. This could mean that the next chunk
   * is larger than the input buffer.
   *
   * Note that this hides valuable information, such as whether a slightly
   * larger buffer could be used to read data. However, providing this
   * information is no guarantee that the next attempt to read will succeed, as
   * a different thread may have read in the meantime.
   *
   * The main use case for I/O queues is to queue packets, which tend to be
   * bounded by the size of packet buffers. In such a case, the maximum
   * possible buffer length is predetermined, and can be adjusted for. This is
   * the case this prototype is optimized for.
   */
  virtual std::size_t pop_front(liberate::types::byte * buf, std::size_t bufsize) = 0;

  inline std::size_t pop_front(void * buf, std::size_t bufsize)
  {
    return pop_front(reinterpret_cast<liberate::types::byte *>(buf), bufsize);
  }


  /**
   * When an entry has been popped but cannot be used, this function can be
   * used to put it back on the queue.
   *
   * Concurrency issues mean that the order of the queue may be changed by
   * the interaction of multiple threads here. This is a risk we have to take.
   *
   * Similarly, a queue implementation may provide only limited space for this
   * operation, in which case push_front() may fail.
   */
  virtual bool push_front(liberate::types::byte const * buf, std::size_t bufsize) = 0;

  inline bool push_front(void const * buf, std::size_t bufsize)
  {
    return push_front(reinterpret_cast<liberate::types::byte const *>(buf),
        bufsize);
  }

protected:
  inline void invoke_data_callback()
  {
    if (!m_data_cb) {
      return;
    }
    m_data_cb(*this);
  }

  data_callback m_data_cb;
};


/**
 * Creator function prototype.
 */
using io_queue_creator = std::function<std::unique_ptr<io_queue> ()>;

} // namespace packeteer::util

#endif // guard
