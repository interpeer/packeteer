/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_CLIENT_H
#define PACKETEER_UTIL_CLIENT_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/scheduler.h>
#include <packeteer/connector.h>

#include <liberate/net/url.h>

#include <packeteer/util/callbacks.h>
#include <packeteer/util/flow.h>
#include <packeteer/util/io_queue.h>
#include <packeteer/util/io_queue/concurrent_memory_queue.h>

namespace packeteer::util {

/**
 * The client class combines basic_client with a flow_manager instanace, I/O
 * queues and a writer that drains the write I/O queue when the underlying
 * socket is writable.
 *
 * The effect of the flow manager is that it abstracts out most of the
 * differences between datagram and stream connectors. The effect of the I/O
 * queues and writer is that writing will always succeed, but possibly be
 * scheduled at a slight delay - transparent to the user.
 *
 * Clients can connect{_to}() multiple endpoints; each is represented by a flow
 * instance. You cannot use a client to read or write; instead, the flow is the
 * interface to use. Use any established() flow for communcation.
 */
class PACKETEER_API client
{
public:
  /**
   * Constructor; needs to take an API instance and scheduler.
   *
   * The timeout is used to determine when a flow should be considered dead.
   * The read_cb is invoked when the endpoint of a flow has sent data. Lastly,
   * the queue_creator is used whenever a new flow is established. You can pass
   * any function returning an io_queue here.
   */
  client(std::shared_ptr<api> api, scheduler & sched, duration timeout,
      timepoint_flow_callback read_cb,
      io_queue_creator queue_creator = create_concurrent_memory_queue
  );

  inline client(std::shared_ptr<api> api, scheduler & sched, duration timeout,
      simple_flow_callback read_cb,
      io_queue_creator queue_creator = create_concurrent_memory_queue
  )
    : client{api, sched, timeout,
        erase_timepoint(read_cb),
        queue_creator
      }
  {
  }


  inline ~client() = default;


  /**
   * The client can optionally take a callback when a peer was detected, or
   * when a close event was issued.
   */
  client & on_remote(simple_flow_callback remote_cb);
  client & on_remote(timepoint_flow_callback remote_cb);

  client & on_close(simple_flow_callback close_cb);
  client & on_close(timepoint_flow_callback close_cb);

  /**
   * Use client to establish a connection to a remote server endpoint.
   *
   * The client allows you to establish multiple connections, either to
   * the same or to remote endpoints. This is not typically how most clients
   * would operate, but there are cases where this is desirable. Note, though,
   * that this means each of the connected() map is essentially independent of
   * the others.
   *
   * We typically prefer to avoid exceptions, but they do make the builder
   * pattern easier to implement. We therefore offer a builder-friendly
   * connect() version of this function, and a exception-avoiding connect_to().
   *
   * These functions perform DNS lookup for connection URLs. They filter the
   * results to matching tuples, e.g. a local IPv4 to a remote IPv4 address,
   * and perform a connection attempt for each matching tuple.
   *
   * Of course, if no local URL is provided, then a "match" is easily found,
   * but the ordering of results is still not determinate.
   */
  error_t connect_to(liberate::net::url const & remote_url, liberate::net::url const & local_url = {});
  client & connect(liberate::net::url const & remote_url, liberate::net::url const & local_url = {});

  template <typename urlT1, typename urlT2 = urlT1>
  error_t connect_to(urlT1 const & remote_url, urlT2 const & local_url = {})
  {
    liberate::net::url local;
    try {
      local = liberate::net::url::parse(local_url);
    } catch (...) {}

    return connect_to(
        liberate::net::url::parse(remote_url),
        local
    );
  }

  template <typename urlT1, typename urlT2 = urlT1>
  client & connect(urlT1 const & remote_url, urlT2 const & local_url = {})
  {
    liberate::net::url local;
    try {
      local = liberate::net::url::parse(local_url);
    } catch (...) {}

    return connect(
        liberate::net::url::parse(remote_url),
        local
    );
  }

  /**
   * Are we connected, yes or no?
   */
  bool is_connected() const;


  /**
   * Access the internal connectors, and the remote URLs they're connected
   * to.
   */
  using connector_map = std::map<peer_address, connector>;
  connector_map const & connected() const;


  /**
   * Map of *established* flows.
   */
  flow_map const & established() const;
  std::size_t num_established() const;


  /**
   * Close a flow. If the last flow via a given connector is closed, the
   * connector is also closed. Conversely, closing the connector closes
   * all associated flows.
   */
  error_t close(flowid const & id);
  error_t close(flow const & the_flow);

private:
  struct client_impl;
  std::shared_ptr<client_impl>  m_impl;
};

} // namespace packeteer::util

#endif // guard
