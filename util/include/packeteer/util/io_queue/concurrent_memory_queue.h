/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_UTIL_IO_QUEUE_CONCURRENT_MEMORY_QUEUE_H
#define PACKETEER_UTIL_IO_QUEUE_CONCURRENT_MEMORY_QUEUE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <packeteer.h>

#include <packeteer/util/io_queue.h>

#include <memory>

namespace packeteer::util {

/**
 * The concurrent_memory_queue implements a concurrently accessible, in-memory
 * queue of chunks of unbounded size.
 *
 * The latter means that:
 * a) chunks are unbounded in size
 * b) the queue is unbounded in size
 * c) the ability to push_front() is unbounded in size.
 *
 * It's also pimpl'd, so provides for light-weight copying, sharing the queue
 * data.
 */
class PACKETEER_API concurrent_memory_queue
  : public io_queue
{
public:
  concurrent_memory_queue();

  concurrent_memory_queue(concurrent_memory_queue const &) = default;
  concurrent_memory_queue(concurrent_memory_queue &&) = default;

  virtual ~concurrent_memory_queue() = default;

  concurrent_memory_queue & operator=(concurrent_memory_queue const &) = default;


  /**
   * See io_queue.
   */
  virtual bool empty() const final;

  /**
   * See io_queue.
   *
   * The function is relatively expensive, and may return inaccurate results.
   */
  virtual ssize_t size() const final;


  /**
   * See io_queue.
   */
  virtual bool push_back(liberate::types::byte const * buf, std::size_t bufsize) final;

  inline bool push_back(void const * buf, std::size_t bufsize)
  {
    return push_back(reinterpret_cast<liberate::types::byte const *>(buf),
        bufsize);
  }


  virtual std::size_t pop_front(liberate::types::byte * buf, std::size_t bufsize) final;

  inline std::size_t pop_front(void * buf, std::size_t bufsize)
  {
    return pop_front(reinterpret_cast<liberate::types::byte *>(buf), bufsize);
  }


  virtual bool push_front(liberate::types::byte const * buf, std::size_t bufsize) final;

  inline bool push_front(void const * buf, std::size_t bufsize)
  {
    return push_front(reinterpret_cast<liberate::types::byte const *>(buf),
        bufsize);
  }

private:
  struct cmq_impl;
  std::shared_ptr<cmq_impl> m_impl;
};


/**
 * Creator function
 */
inline std::unique_ptr<io_queue>
create_concurrent_memory_queue()
{
  std::unique_ptr<io_queue> ptr{new concurrent_memory_queue()};
  return ptr;
}


} // namespace packeteer::util

#endif // guard
