/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef PACKETEER_TEST_COMPARE_TIMES_H
#define PACKETEER_TEST_COMPARE_TIMES_H

#include <gtest/gtest.h>

#include <chrono>

template <typename firstT, typename secondT, typename expectedT>
inline void
compare_times(firstT const & first, secondT const & second, expectedT const & expected)
{
  namespace sc = std::chrono;

  auto diff = sc::duration_cast<sc::nanoseconds>(second.time_since_epoch()) -
    sc::duration_cast<sc::nanoseconds>(first.time_since_epoch());

  ASSERT_GT(diff.count(), 0) << "Cannot spend negative time!";

  // The diff should be no more than 25% off of the expected value, but also it's possible it will be >20ms due to scheduler granularity.
  auto max = sc::duration_cast<sc::nanoseconds>(expected * 1.25);
  auto twenty = sc::milliseconds(20);
  if (max < twenty) {
    max = twenty;
  }

  EXPECT_LE(diff.count(), max.count()) << "Should not fail except under high CPU workload or on emulators!";
}

#endif // guard
