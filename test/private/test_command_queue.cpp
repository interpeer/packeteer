/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/connector/types.h>

#include <gtest/gtest.h>

#include <string>

#include "../lib/command_queue.h"

namespace p7r = packeteer;
namespace pd = packeteer::detail;

TEST(DetailCommandQueue, enqueue_and_dequeue)
{
  using test_queue = pd::command_queue<int, std::string>;

  test_queue tq;

  tq.enqueue(42, "Hello");
  tq.enqueue(123, "world");

  int command = 0;
  std::string arg;

  bool ret = tq.dequeue(command, arg);
  ASSERT_TRUE(ret);
  ASSERT_EQ(42, command);
  ASSERT_EQ("Hello", arg);

  ret = tq.dequeue(command, arg);
  ASSERT_TRUE(ret);
  ASSERT_EQ(123, command);
  ASSERT_EQ("world", arg);

  ret = tq.dequeue(command, arg);
  ASSERT_FALSE(ret);
}



TEST(DetailCommandQueue, copy_counting)
{
  struct test
  {
    inline test() {}
    inline test(test const & t) { copies = t.copies + 1; };
    inline test & operator=(test const &) = default;
    int copies = 0;
  };

  using test_queue = pd::command_queue<int, test>;

  test_queue tq;
  tq.enqueue(42, test{});

  int command;
  test result;
  bool ret = tq.dequeue(command, result);
  ASSERT_TRUE(ret);

  // Copy for enqueueing, and copy for dequeueing.
  ASSERT_EQ(2, result.copies);
}



TEST(DetailCommandQueueWithSignal, signalling)
{
  using test_queue = pd::command_queue_with_signal<int, std::string>;

  p7r::connector conn{p7r::api::create(), "anon://"};
  auto err = conn.connect();
  ASSERT_EQ(p7r::ERR_SUCCESS, err);

  test_queue tq{conn};

  tq.enqueue(42, "foo"); // does not really matter
  tq.commit();

  auto ret = tq.clear();
  ASSERT_TRUE(ret);

  // The interrupt can be cleared and queried independent of whether
  // the queue has entries.
}
