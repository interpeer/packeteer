/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "../../lib/util/string.h"

#include <gtest/gtest.h>


TEST(UtilString, to_lower)
{
  namespace pu = packeteer::util;

  ASSERT_EQ("foo", pu::to_lower("foo"));
  ASSERT_EQ("foo", pu::to_lower("Foo"));
  ASSERT_EQ("foo", pu::to_lower("fOo"));
  ASSERT_EQ("foo", pu::to_lower("foO"));
  ASSERT_EQ("foo", pu::to_lower("FOO"));

  ASSERT_EQ("", pu::to_lower(""));
  ASSERT_EQ("a", pu::to_lower("A"));
}


TEST(UtilString, to_upper)
{
  namespace pu = packeteer::util;

  ASSERT_EQ("FOO", pu::to_upper("foo"));
  ASSERT_EQ("FOO", pu::to_upper("Foo"));
  ASSERT_EQ("FOO", pu::to_upper("fOo"));
  ASSERT_EQ("FOO", pu::to_upper("foO"));
  ASSERT_EQ("FOO", pu::to_upper("FOO"));

  ASSERT_EQ("", pu::to_upper(""));
  ASSERT_EQ("A", pu::to_upper("a"));
}


TEST(UtilString, case_insensitive_search)
{
  namespace pu = packeteer::util;

  auto res = pu::ifind("This is a Test String", "test");
  ASSERT_GE(res, 0); // Less than zero would be failure

  // "test" is at offset 10
  ASSERT_EQ(10, res);

  // Find something at the beginning and end of strings
  ASSERT_EQ(0, pu::ifind("foobar", "FOO"));
  ASSERT_EQ(3, pu::ifind("foobar", "Bar"));

  // Return -1 if string can't be found
  ASSERT_EQ(-1, pu::ifind("foobar", "quux"));
  ASSERT_EQ(-1, pu::ifind("quu", "quux"));
  ASSERT_EQ(-1, pu::ifind("", "quux"));

  // Find the empty string anywhere, so at 0.
  ASSERT_EQ(0, pu::ifind("foobar", ""));
  // .. except in an empty string
  ASSERT_EQ(-1, pu::ifind("", ""));
}


TEST(UtilString, replace)
{
  namespace pu = packeteer::util;

  ASSERT_EQ("foo", pu::replace("f0o", "0", "o"));
  ASSERT_EQ("fo0", pu::replace("f00", "0", "o", true));

  ASSERT_EQ("\\\\quoted\\\\and\\\\separated\\\\",
      pu::replace("\\quoted\\and\\separated\\", "\\", "\\\\"));
}


TEST(UtilString, urlencode)
{
  namespace pu = packeteer::util;

  ASSERT_EQ("foo/bar", pu::urlencode("foo/bar"));
  ASSERT_EQ("/%7Efoo/bar", pu::urlencode("/~foo/bar"));
  ASSERT_EQ("%00abstract", pu::urlencode(std::string{"\0abstract", 9}));
  ASSERT_EQ("%25asdf", pu::urlencode("%asdf"));
}



TEST(UtilString, urldecode)
{
  namespace pu = packeteer::util;

  ASSERT_EQ(pu::urldecode("foo/bar"), "foo/bar");
  ASSERT_EQ(pu::urldecode("/%7Efoo/bar"), "/~foo/bar");
  ASSERT_EQ(pu::urldecode("%00abstract"), (std::string{"\0abstract", 9}));
  ASSERT_EQ(pu::urldecode("%25asdf"), "%asdf");
}
