/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "../../lib/connector/win32/socketpair.h"
#include "../../lib/connector/win32/socket.h"
#include "../../lib/macros.h"

#include <gtest/gtest.h>

#include "../test_name.h"

namespace p7r = packeteer;
namespace pd = p7r::detail;

namespace {

inline void
socketpair_test(int domain)
{
  SOCKET socks[2] = { INVALID_SOCKET, INVALID_SOCKET };

  auto err = pd::socketpair(domain, SOCK_STREAM, 0, socks);
  ASSERT_EQ(p7r::ERR_SUCCESS, err);
  ASSERT_NE(socks[0], INVALID_SOCKET);
  ASSERT_NE(socks[1], INVALID_SOCKET);
  ASSERT_NE((HANDLE) socks[0], INVALID_HANDLE_VALUE);
  ASSERT_NE((HANDLE) socks[1], INVALID_HANDLE_VALUE);

  // Write on client, read on server.
  char buf[] = { 42 };
  DWORD written = 0;
  OVERLAPPED o{};
  BOOL res = WriteFile((HANDLE) socks[1], (void *) buf, sizeof(buf), &written, &o);
  ASSERT_TRUE(res);
  ASSERT_EQ(written, 1);

  buf[0] = 0;
  DWORD read = 0;
  res = ReadFile((HANDLE) socks[0], (void *) buf, sizeof(buf), &read, &o);
  ASSERT_TRUE(res);
  ASSERT_EQ(read, 1);
  ASSERT_EQ(buf[0], 42);

  // Cleanup
  pd::close_socket(socks[0]);
  pd::close_socket(socks[1]);
}


} // anonymous namespace



TEST(SocketPair, create_inet)
{
  socketpair_test(AF_INET);
}


TEST(SocketPair, create_inet6)
{
  socketpair_test(AF_INET6);
}


TEST(SocketPair, create_local)
{
  socketpair_test(AF_UNIX);
}
