/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2019-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef PACKETEER_TEST_TEST_NAME_H
#define PACKETEER_TEST_TEST_NAME_H

#define REPLACE_HELPER(c, replacement) \
  case c: \
    res += replacement; break ;

inline std::string
symbolize_name(std::string const & other)
{
  // std::cout << "NAME IS: >" << other << "< - " << other.size() << std::endl;
  std::string res;

  if (other.empty()) {
    res = "_empty_";
  }
  else {
    // Replace non-symbol with a sequence
    for (auto c : other) {
      switch (c) {
        REPLACE_HELPER('\0', "_null_");
        REPLACE_HELPER('.', "_dot_");
        REPLACE_HELPER(':', "_colon_");
        REPLACE_HELPER('/', "_slash_");
        REPLACE_HELPER('[', "_open_");
        REPLACE_HELPER(']', "_close_");
        REPLACE_HELPER(' ', "_");
        REPLACE_HELPER('\\', "_backslash_");
        REPLACE_HELPER('+', "_plus_");
        REPLACE_HELPER('-', "_minus_");
        REPLACE_HELPER('%', "_percent_");

        default:
          res += c;
          break;
      }
    }

    // Replace consecutive _ with a single
    auto len = res.length();
    for (size_t i = 0 ; i < len - 1 ; ++i) {
      if (res[i] != '_') continue;

      for (size_t j = i + 1 ; j < len - 1 ; ++j) {
        if (res[j] == '_') {
          res.erase(res.begin() + j);
          len = res.length();
        } else {
          break;
        }
      }
    }
  }

  return res;
}

#endif // guard
