/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2017-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <packeteer/error.h>

#include <errno.h>

#include <string>

#include <gtest/gtest.h>


TEST(Error, basics)
{
  try {
    throw packeteer::exception(packeteer::ERR_SUCCESS);
  } catch (packeteer::exception const & ex) {
    ASSERT_EQ(packeteer::ERR_SUCCESS, ex.code());
    auto msg = std::string{ex.what()};
    ASSERT_NE(std::string::npos, msg.find("No error"));
    ASSERT_EQ(std::string("ERR_SUCCESS"), std::string(ex.name()));
  }
}


TEST(Error, details_without_errno)
{
  // Without errno
  try {
    throw packeteer::exception(packeteer::ERR_SUCCESS, "foo");
  } catch (packeteer::exception const & ex) {
    auto msg = std::string{ex.what()};
    ASSERT_NE(std::string::npos, msg.find(" // foo"));
  }
}


TEST(Error, details_with_errno)
{
  // With an errno value
#ifdef PACKETEER_WIN32
#define ERROR_CODE ERROR_INVALID_HANDLE
#else
#define ERROR_CODE EAGAIN
#endif
  try {
    throw packeteer::exception(packeteer::ERR_SUCCESS, ERROR_CODE, "foo");
  } catch (packeteer::exception const & ex) {
    auto msg = std::string{ex.what()};
    ASSERT_NE(std::string::npos, msg.find("[ERR_SUCCESS] "));
    ASSERT_NE(std::string::npos, msg.find("// foo"));
  }
}
