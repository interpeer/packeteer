/**
 * This file is part of packeteer.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <packeteer/util/path.h>

#include <gtest/gtest.h>

#include "../test_name.h"

namespace {

struct test_data_from_win32
{
  std::string name;
  std::string canonical_posix;
  std::string input_win32;
} tests_from_win32[] = {
  { "drive letters",
    "/c", "C:", },
  { "drive letters with delim",
    "/c", "C:\\", },

  { "root paths",
    "/foo", "\\foo", },
  { "root paths with delim",
    "/foo", "\\foo\\", },

  { "longer path",
    "/c/tmp/foo", "C:\\tmp\\foo", },
  { "longer path with delim",
    "/c/tmp/foo", "C:\\tmp\\foo\\", },

  { "quoted delimiter",
    "/c/tmp\/foo", "C:\\tmp\/foo", },
};


struct test_data_from_posix
{
  std::string name;
  std::string canonical_win32;
  std::string input_posix;
} tests_from_posix[] = {
  { "drive letters",
    "C:", "/c", },
  { "drive letters with delim",
    "C:", "/c/", },

  { "root paths",
    "\\foo", "/foo", },
  { "root paths with delim",
    "\\foo", "/foo/", },

  { "longer path",
    "C:\\tmp\\foo", "/c/tmp/foo", },
  { "longer path with delim",
    "C:\\tmp\\foo", "/c/tmp/foo/", },

  { "quoted delimiter",
    "C:\\tmp\\\\foo", "/c/tmp\\foo", },
};


template <typename T>
std::string generate_name(testing::TestParamInfo<T> const & info)
{
  return symbolize_name(info.param.name);
}

} // anonymous namespace


class UtilPathFromWin32
  : public testing::TestWithParam<test_data_from_win32>
{
};



TEST_P(UtilPathFromWin32, convert)
{
  auto td = GetParam();

  auto posix = packeteer::util::to_posix_path(td.input_win32);
  ASSERT_EQ(td.canonical_posix, posix);
}


INSTANTIATE_TEST_SUITE_P(util, UtilPathFromWin32, testing::ValuesIn(tests_from_win32),
    generate_name<test_data_from_win32>);


class UtilPathFromPosix
  : public testing::TestWithParam<test_data_from_posix>
{
};


TEST_P(UtilPathFromPosix, convert)
{
  auto td = GetParam();

  auto win32 = packeteer::util::to_win32_path(td.input_posix);
  ASSERT_EQ(td.canonical_win32, win32);
}


INSTANTIATE_TEST_SUITE_P(util, UtilPathFromPosix, testing::ValuesIn(tests_from_posix),
    generate_name<test_data_from_posix>);

